module Storable
  def self.included(base)
    base.extend Storable::ActivateMethod
  end

  module ActivateMethod
    def storable_on *attrs
      self.cattr_accessor :_storable
      self._storable = {
        attributes: attrs
      }
      attrs.each do |attr|
        self.after_destroy "delete_#{attr}".to_sym
      end
      self.extend Storable::ClassMethods
      self.__send__ :include, Storable::InstanceMethods
    end
  end

  module ClassMethods
  end

  module InstanceMethods
    def self.included klass
      klass._storable[:attributes].each do |attr|
        define_method "#{attr}_key" do
          "#{self.class.name}_#{self.id}_#{attr}"
        end

        define_method attr do
          MatomeChannelFS.get self.send("#{attr}_key")
        end

        define_method "#{attr}=" do |data|
          MatomeChannelFS.set self.send("#{attr}_key"), data
        end

        define_method "#{attr}_file=" do |data|
          MatomeChannelFS.set_file self.send("#{attr}_key"), data
        end

        define_method "#{attr}_paths" do
          MatomeChannelFS.paths self.send("#{attr}_key")
        end

        define_method "delete_#{attr}" do
          MatomeChannelFS.delete self.send("#{attr}_key")
        end
      end
    end
  end

end

