class BlackHost
  include Mongoid::Document
  include Mongoid::Timestamps

  # fields
  field :target, type: String
  field :reporter, type: String

end
