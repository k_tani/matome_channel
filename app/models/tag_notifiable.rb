module TagNotifiable
  def self.included base
    base.has_many :tag_observers
    base.extend TagNotifiable::ClassMethods
    base.__send__ :include, TagNotifiable::InstanceMethods 
  end

  module ClassMethods
  end

  module InstanceMethods
    def notify data
      observer_ids = self.tag_observers.map(&:id)
      Settings.ws_servers.each do |wss|
        TagChangeNotify.namespace(wss.name) do |bcn|
          Resque.enqueue bcn, observer_ids, data if observer_ids.any?
        end
      end
    end
  end
end

