class Configuration
  include Mongoid::Document

  # fields
  field :key, type: String
  field :value, type: String

  # relations
  embedded_in :board

  attr_accessor :name
end
