class AnonymousUser

  include AbstractUser

  attr_accessor :id, :name, :refer_history

  def initialize session
    @id = nil
    @name = :guest
    @session = session
    @session[:ng_list] ||= {}
    @session[:check_list] ||= {}
    @refer_history = []
  end

  def get_ng_list
    @session[:ng_list]
  end

  def add_ng_list comment
    @session[:ng_list][comment.hash_id] = true
  end

  def remove_ng_list comment
    @session[:ng_list].delete comment.hash_id
  end

  def add_check_list comment, val=1
    @session[:check_list][comment.id] = val
  end

  def ng? comment
    !!@session[:ng_list][comment.hash_id]
  end

  def checked? comment
    !!@session[:check_list][comment.id]
  end

  def comment_template comment
    if ng?(comment)
      "comments/ng_comment"
    else
      "comments/ok_comment"
    end
  end

  def fixed_handle_name
    ''
  end
end
