class HashIdMap
  include Mongoid::Document

  SECRET = "58518f39d2436309f325d62d3095c084beb9997f"

  HASH_LENGTH = 16

  # fields
  field :hash_id, type: String
  field :comment_ids, type: Array, default: []

  # relations
  belongs_to :board

  # indexes
  index({hash_id: 1}, {name: "hash_id_index"})
  index({board_id: 1, hash_id: 1}, {name: "board_id_hash_id_index"})

  def self.create_hash_id comment
    date = comment.created_at.try(:to_date)
    date ||= Date.today
    digest = Base64.encode64(Digest::SHA1.digest("#{SECRET}#{date}#{comment.remote_host}#{comment.user_agent}"))
    hash_id = digest[0...HASH_LENGTH]
    hash_id
  end

  def self.add_comment comment
    hid= self.where(hash_id: comment.hash_id, board_id: comment.board_id).first
    if hid
      hid.push :comment_ids, comment.id unless hid.comment_ids.include?(comment.id)
    else
      hid = self.new{|h|
        h.hash_id = comment.hash_id
        h.board = comment.board
        h.comment_ids = [comment.id]
      }.save
    end
  end

  def self.get_map hash_ids, board_id=nil
    distributions = self.where(:hash_id.in => hash_ids)
    distributions = distributions.where(board_id: board_id) if board_id
    map = {}
    distributions.each do |d|
      map[d.hash_id] = d.comment_ids
    end
    map
  end

end
