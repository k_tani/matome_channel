# -*- coding: utf-8 -*-
class SummaryTag
  include Mongoid::Document
  include Mongoid::Timestamps
  include TagNotifiable

  field :name, type: String
  field :used_count, type:Integer, default: 0

  # relations
  has_and_belongs_to_many :summaries

  # indexes
  index({ name: 1 }, { unique: true, name: "name_index" })

  # 最新３個のデータ
  def popular_summaries
    self.summaries.desc(:refered_count).limit(3)
  end

  #タグ側のサムネイル
  def thumbnail
    if (summary = self.summaries.first)
      summary.thumbnail
    else
      'placeholder.png'
    end
  end

  def notify_new_summary html
    self.notify(({
      type: :new_summary,
      html: html
    }).to_json);
  end

  def notify_update_summary html
    self.notify(({
      type: :update_summary,
      html: html
    }).to_json);
  end

end
