# encoding: utf-8
class Board
  include Mongoid::Document
  include Mongoid::Timestamps
  include Taggable

  taggable_on BoardTag, separator: ','

  # fields
  field :title, type: String
  field :count, type: Integer
  field :score, type: Integer, default: 0

  # indexes
  index({ score: -1 }, { name: "score_index" })
  index({ count: -1 }, { name: "count_index" })
  index({ observer_ids: 1 }, { name: "observer_ids_index" })
  index({ tag_ids: 1 }, { name: "tag_ids_index" })

  # relation
  has_many :comments, order: {created_at: :asc}
  has_many :hash_id_maps
  embeds_many :configs, class_name: "Configuration"
  belongs_to :category
  belongs_to :user
  has_and_belongs_to_many :observers, class_name: 'BoardObserver'
  has_many :board_images
  has_many :board_websites
  has_many :board_movies
  has_many :favorites, class_name: "BoardFavorite"
  accepts_nested_attributes_for :comments
  accepts_nested_attributes_for :configs

  # validation
  validates :title, :length => {:in => 1..255}

  # attributes
  attr_accessible :score, :title

  # configuration keys
  CONFIGS = {
    default_name: {name: "デフォルトネーム",  default: "名無しさん。"}
  }

  def first_comment
    self.comments.order_by(created_at: :asc).limit(1).first
  end

  def build_config
    Board::CONFIGS.each do |k, c|
      self.configs << Configuration.new({key: k, name: c[:name], value: c[:default]})
    end
    self.configs
  end

  def thumbnail
    image = self.board_images.order_by(count: :desc, created_at: :desc).limit(1).first
    website = self.board_websites.order_by(count: :desc, created_at: :desc).limit(1).first
    movie = self.board_movies.order_by(count: :desc, created_at: :desc).limit(1).first
    targets = [website,movie,image]
    target = targets.sort!{|a,b| a.try(:count).to_i <=> b.try(:count).to_i}.last
    case target
    when BoardWebsite
      target.website
    when BoardImage
      target.image
    when BoardMovie
      target.movie
    else
      'placeholder.png'
    end
  end

  def config
    return @config unless @config.blank?
    @config = HashWithIndifferentAccess.new
    configs.each do |c|
      @config[c.key] = c.value
    end
    @config
  end

  def inc_count num=1
    self.inc(:count, num)
  end

  def inc_score num=1
    self.inc(:score, num)
  end

  def add_image image
    board_image = BoardImage.where(image_id: image.id, board_id: self.id).first
    if board_image
      count = board_image.inc_count
      self.notify(({
        type: :change_board_image_count,
        count: count,
      }).to_json)
    else
      self.board_images << BoardImage.new(image_id: image.id)
      board_score = self.inc_score
      self.notify(({
        type: :add_board_image,
        board_score: board_score,
        html: image.to_html
      }).to_json)
    end
  end

  def add_website website
    board_website = BoardWebsite.where(website_id: website.id, board_id: self.id).first
    if board_website
      count = board_website.inc_count
      board_score = self.inc_score
      self.notify(({
        type: :change_board_website_count,
        count: count,
      }).to_json)
    else
      self.board_websites << BoardWebsite.new(website_id: website.id)
      board_score = self.inc_score
      self.notify(({
        type: :add_board_website,
        board_score: board_score,
        html: website.to_html
      }).to_json)
    end
  end

  def add_movie movie
    board_movie = BoardMovie.where(movie_id: movie.id, board_id: self.id).first
    if board_movie
      count = board_movie.inc_count
      board_score = self.inc_score
      self.notify(({
        type: :change_board_movie_count,
        count: count,
      }).to_json)
    else
      self.board_movies << BoardMovie.new(movie_id: movie.id)
      board_score = self.inc_score
      self.notify(({
        type: :add_board_movie,
        board_score: board_score,
        html: movie.to_html
      }).to_json)
    end
  end

  def notify_change_favorites
    self.notify(({
      type: :change_favorites,
      count: self.favorites.count
    }).to_json)
  end

  def notify_change_tags html
    self.notify(({
      type: :change_board_tags ,
      html: html}).to_json)
  end

  def notify data
    Settings.ws_servers.each do |wss|
      BoardChangeNotify.namespace(wss.name) do |bcn|
        Resque.enqueue bcn, self.observer_ids, data if self.observer_ids.any?
      end
    end
  end

end
