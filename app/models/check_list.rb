class CheckList
  include Mongoid::Document

  belongs_to :user

  field :check_list, type: Hash, default: {}

  
end
