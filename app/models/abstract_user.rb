module AbstractUser

  def get_ng_list
    raise "get_ng_list is not overrided."
  end

  def add_ng_list comment
    raise "add_ng_list is not overrided."
  end

  def remove_ng_list comment
    raise "remove_ng_list is not overrided."
  end

  def add_check_list comment, val=1
    raise "add_check_list is not overrided."
  end

  def ng? comment
    raise "ng? is not overrided."
  end

  def checked? comment
    raise "checked? is not overrided."
  end

  def comment_template comment
    if ng?(comment)
      "comments/ng_comment"
    else
      "comments/ok_comment"
    end
  end
end

