class BoardMovie
  include Mongoid::Document
  include Mongoid::Timestamps

  COUNT_PER_PAGE = 50

  # fields
  field :count, type: Integer, default: 1

  # relations
  belongs_to :board
  belongs_to :movie

  def inc_count num=1
    self.inc(:count, num)
  end

end
