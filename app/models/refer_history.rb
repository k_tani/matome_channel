class ReferHistory
  include Mongoid::Document

  # constants
  MaxHistory = 100

  # relations
  belongs_to :user

  # fields
  field :board_histories, type: Array, default: []
  field :summary_histories, type: Array, default: []

  alias_method "<<", :push

  def push object
    case object
    when Board, Summary
      target = "#{object.class.name.underscore}_histories".to_sym
      self.pull target, ({id: object.id})
      currents = self.send target
      self.pull target, currents.first if currents.length >= MaxHistory
      super target, ({id: object.id, title: object.title, refered_at: Time.now})
    else
      # no process
      logger.warn "unknown object #{object.class}"
    end
  end

  def boards
    Kaminari::paginate_array(board_histories.reverse)
  end

  def summaries
    Kaminari::paginate_array(summary_histories.reverse)
  end

end
