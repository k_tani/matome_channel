class User
  include ::AbstractUser
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:twitter, :facebook, :google_oauth2]

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  # fixed handle name
  field :use_fix_handle_name, :type => Boolean, :default => false

  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  ## Omni
  field :uid, type: String
  field :name, type: String
  field :provider, type: String

  #relations
  has_one :check_list
  has_one :ng_list
  has_one :refer_history
  has_many :comments
  has_many :boards
  has_many :summaries
  has_many :board_favorites
  has_many :summary_favorites

  #callbacks
  after_create :create_check_list
  after_create :create_ng_list
  after_create :create_refer_history

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"]
      end
    end
  end


  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    user ||= User.create(name:auth.extra.raw_info.name,
                         provider:auth.provider,
                         uid:auth.uid,
                         password:Devise.friendly_token[0,20])
    user
  end


  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    user ||= User.create(name:auth.info.nickname,
                         provider:auth.provider,
                         uid:auth.uid,
                         password:Devise.friendly_token[0,20])
    user
  end

  def self.find_for_google_oauth2(auth, signed_in_resource=nil)
    p auth
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    user ||= User.create(name: auth.info.name,
             password: Devise.friendly_token[0,20],
             provider: auth.provider,
             uid: auth.uid
            )
    user
  end

  def email_required?
    super && provider.blank?
  end

  def get_ng_list
    self.ng_list.ng_list
  end

  def get_check_list
    self.check_list.check_list
  end

  def add_ng_list comment
    cl = self.ng_list
    cl.ng_list[comment.hash_id.to_s] = true
    cl.save
  end

  def remove_ng_list comment
    cl = self.ng_list
    cl.unset("ng_list.#{comment.hash_id.to_s}")
  end

  def add_check_list comment, val=1
    uc = self.check_list
    uc.check_list[comment.id.to_s] = val
    uc.save
  end

  def ng? comment
    NgList.where(user_id: self.id, "ng_list.#{comment.hash_id}" => true).any?
  end

  def checked? comment
    CheckList.where(user_id: self.id, "check_list.#{comment.id}".to_sym.ne => nil).any?
  end

  def create_check_list
    CheckList.new {|uc|uc.user = self}.save
  end

  def create_ng_list
    NgList.new {|uc|uc.user = self}.save
  end

  def create_refer_history
    ReferHistory.new {|uc|uc.user = self}.save
  end

  def favorite? object
    case object
    when Board
      self.board_favorites.where(:board_id => object.id).first
    when Summary
      self.summary_favorites.where(:summary_id => object.id).first
    else
      false
    end
  end

  def mine? object
    case object
    when Summary
      object.user == self
    else
      false
    end
  end

  def fixed_handle_name
    self.use_fix_handle_name ? self.name : ''
  end
end
