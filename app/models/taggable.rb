# encoding: utf-8
# mongoid_taggableとか使う手もあったけど、
# これらはembbed_inとかで実際のドキュメント内に
# データが持たれる。これだと若干重いかなーとおもってこうした。
module Taggable
  def self.included(base)
    base.extend Taggable::ActivateMethod
  end

  module ActivateMethod
    def taggable_on klass, options={}
      throw "undefine class" if klass.nil?
      options[:separator] ||= ','
      self.cattr_accessor :_taggable
      self._taggable = {
        klass: klass,
        separator: options[:separator],
      }
      self.has_and_belongs_to_many :tags, class_name: klass.to_s,
        :after_add => :_tag_added, 
        :after_remove => :_tag_removed
      self.accepts_nested_attributes_for :tags
      self.extend Taggable::ClassMethods
      self.__send__ :include, Taggable::InstanceMethods
    end
  end

  module ClassMethods
    def tagged_with tag
      tag_ids = self._taggable[:klass].where(name: tag).map(&:id)
      self.where(:tag_ids.in => tag_ids)
    end
  end

  module InstanceMethods
    def tag_list
      self.tags.map(&:name).join(self.class._taggable[:separator])
    end

    def build_tags text
      list = []
      build_each_tag(text) do |tag|
        list << tag
        yield tag if block_given?
      end
      self.tags = list
    end

    def add_tags text
      build_each_tag(text) do |tag|
        self.tags << tag
        yield tag if block_given?
      end
    end

    def build_each_tag text
      sep = self.class._taggable[:separator]
      case text
      when String
        text = text.split sep
      when Array
        # no process
      else
        raise "argument is not Array or String"
      end
      list = []
      klass = self.class._taggable[:klass]
      text.each do |t|
        t.strip!
        tag = klass.where(name: t).first
        tag = klass.new(name: t) if tag.nil?
        yield tag
      end
    end

    def remove_tags text
      sep = self.class._taggable[:separator]
      klass = self.class._taggable[:klass]
      case text
      when klass
        text = [text]
      when String
        text = text.split sep
      when Array
        # no process
      else
        raise "argument is not Array or String"
      end
      list = []
      self_ids = "#{self.class.to_s.underscore}_ids".to_sym
      text.each do |t|
        case t
        when klass
          self.tags.delete(t)
        when String
          tag = klass.where(name: t).first
          if tag
            self.tags.delete(tag)
          end
        end
      end
    end

    def _tag_added tag
      tag.inc :used_count, 1
    end

    def _tag_removed tag
      tag.inc :used_count, -1
    end
  end
end

