class NgList
  include Mongoid::Document

  belongs_to :user

  field :ng_list, type: Hash, default: {}
end
