class TagObserver
  include Mongoid::Document

  belongs_to :board_tag
  belongs_to :comment_tag
  belongs_to :summary_tag

  index({board_tag_id: 1}, {name: :board_tag_id})
  index({comment_tag_id: 1}, {name: :comment_tag_id})
  index({summary_tag_id: 1}, {name: :summary_tag_id})

end
