class Summary
  include Mongoid::Document
  include Mongoid::Timestamps
  include Taggable

  taggable_on SummaryTag, separator: ','

  ConditionKey = {
    good_gte:       {attr: :good,       name: "ｲｲﾈ!",     selector: "$gte"},
    good_lte:       {attr: :good,       name: "ｲｲﾈ!",     selector: "$lte"},
    bad_gte:        {attr: :bad,        name: "ｲｸﾅｲ!",    selector: "$gte"},
    bad_lte:        {attr: :bad,        name: "ｲｸﾅｲ!",    selector: "$lte"},
    tag_in:         {attr: :tag_ids,    name: "タグ",     selector: "$in"},
    hash_id_in:     {attr: :hash_id,    name: "ID",       selector: "$in"},
    created_at_gte: {attr: :created_at, name: "投稿日時", selector: "$gte"},
    created_at_lte: {attr: :created_at, name: "投稿日時", selector: "$lte"},
  }

  FontSizes = [8, 9, 10, 11, 12, 14, 16, 18, 24, 36, 48, 64]
  DefaultDecoration = {
    size: "14",
    color: "#333333",
    bold: false,
    italic:  false,
    comment: ""
  }

  # fields
  field :title, type: String
  field :description, type: String
  field :publish, type: Boolean, default: false
=begin
item: {
  commment_id: id (ObjectId)
  decoration:{
    size: "14px" (String)
    color: "#ffffff" (String)
    bold: true / false (Boolean)
    italic:  true / false (Boolean)
    comment: "hogehoge" (String)
  }
}
=end
  field :items, type: Array, default: []
  field :refered_count, type: Integer, default: 0

  # relations
  belongs_to :category
  belongs_to :board
  belongs_to :user
  has_many :favorites, class_name: "SummaryFavorite"

  # validations
  validates :title, length: {in: 1..255}
  validates :description, length: {in: 1..1023}

  def comments
    Comment.where(:id.in => self.items.map{|i|i["comment_id"]})
  end

  def thumbnail
    comment_ids = self.comments.map(&:id)
    image = Image.where(:comment_ids.in => comment_ids).order_by(created_at: :desc).first
    website = Website.where(:comment_ids.in => comment_ids).order_by(created_at: :desc).first
    movie =  Movie.where(:comment_ids.in => comment_ids).order_by(created_at: :desc).first
    targets = [website,movie,image]
    target = targets.sort!{|a,b| a.try(:created_at).to_i <=> b.try(:created_at).to_i}.last
    if target
      target
    else
      'placeholder.png'
    end
  end

  def embed_items
    ret = []
    comment_hash = {}
    self.comments.each{|c|comment_hash[c.id.to_s] = c}
    self.items.each do |item|
      deco = Struct.new(*(item["decoration"].keys.map(&:to_sym))).new(*(item["decoration"].values))
      ret << Struct.new(:comment, :decoration).new(comment_hash[item["comment_id"]], deco)
    end
    ret
  end

  def get_items conditions
    comments = self.board.comments.order_by(num: :asc, created_at: :asc)
    and_conds = []
    build_conditions(conditions).each do |conds|
      or_conds = []
      conds.each do |key, val|
        c = Summary::ConditionKey[key.to_sym]
        or_conds << ({c[:attr] => { c[:selector] => val }}) unless c.nil?
      end
      and_conds << ({"$or" => or_conds})
    end
    items = []
    deco = Struct.new(*(DefaultDecoration.keys.map(&:to_sym))).new(*(DefaultDecoration.values))
    comments.where("$and" => and_conds).each do |c|
      items << Struct.new(:comment, :decoration).new(c, deco)
    end
    items
  end

  def private?
    !self.publish
  end

  def publish?
    self.publish
  end

  private

  def build_conditions conditions
    ret = []
    conditions.map do |i,conds|
      cond = {}
      conds.each do |k,v|
        case k
        when "tag_in"
          cond[k] = CommentTag.where(:name.in => v.split(',')).map(&:id)
        when "hash_id_in"
          cond[k] = v.split ","
        else
          cond[k] = v
        end
      end
      ret << cond
    end
    ret
  end

end
