class BoardFavorite
  include Mongoid::Document
  include Mongoid::Timestamps

  # relations
  belongs_to :user
  belongs_to :board
end
