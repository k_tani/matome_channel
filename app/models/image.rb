class Image
  include Mongoid::Document
  include Mongoid::Timestamps
  include Storable

  storable_on :image, :thumb

  # fields
  field :url, type: String
  field :content_type, type: String

  # relation
  has_and_belongs_to_many :comments

  # indexes
  index({ url: 1 }, { name: "url_index" })

  # attrs
  attr_accessor :upload_file

  def to_thumb_html
    get_view.render(
      :partial => "images/thumb",
      :locals => {:image => self})
  end

  def to_html
    get_view.render(
      :partial => "images/image",
      :locals => {:image => self})
  end

  def hash_ids
    self.comments.map(&:hash_id).uniq
  end

  def ext_name
    content_type.split("/").last
  end

  private
  def get_view
    view = ActionView::Base.new(Rails.configuration.paths["app/views"].first)
    class << view
      include ImagesHelper
      include Rails.application.routes.url_helpers
    end
    view
  end

end
