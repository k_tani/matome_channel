class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  include Taggable

  COUNT_PER_PAGE = 50

  taggable_on CommentTag, separator: ','

  # fields
  field :num, type: Integer, default: 0
  field :name, type: String
  field :body, type: String
  field :good, type: Integer, default: 0
  field :bad, type: Integer, default: 0
  field :remote_host, type: String
  field :user_agent, type: String
  field :hash_id, type: String

  # indexes
  index({ num: 1 }, { name: "num_index" })
  index({ board_id: 1 }, { name: "board_id_index" })
  index({ hash_id: 1 }, { name: "hash_id_index" })
  index({ good: -1 }, { name: "good_index" })
  index({ bad: 1 }, { name: "bad_index" })

  # relation
  belongs_to :board
  belongs_to :user
  has_and_belongs_to_many :websites
  has_and_belongs_to_many :images
  has_and_belongs_to_many :movies

  # validation
  validates :name, length: {in: 0..255}
  validates :body, length: {in: 1..2048}
  validate :check_remote_host
  validate :check_hash_id

  # attributes
  attr_accessible :bad, :body, :good, :name, :remote_host, :user_agent, :board_id, :parent_id, :hash_id

  # callbacks
  before_validation :complete_name, on: :create
  before_validation :create_hash_id, on: :create
  after_create :create_num

  def complete_name
    self.name = self.board.config[:default_name] if self.name.blank?
  end

  def create_hash_id
    self.hash_id = HashIdMap.create_hash_id self
  end

  def create_num
    self.num = self.board.inc_count
    HashIdMap.add_comment self
    self.save
    Resque.enqueue CommentParse, self.id, self.body
    board_score = self.board.inc_score(1)
  end

  def notify_new_comment html, hash_id_count 
    self.board.notify ({
      type: :new_comment ,
      board_score: self.board.score,
      id: self.id,
      hash_id: self.hash_id,
      hash_id_count: hash_id_count,
      html: html}).to_json
  end

  def inc_good
    board_score = self.board.inc_score(1)
    good_score = self.inc(:good, 1)
    self.board.notify(({
      type: :change_good,
      comment_id: self.id,
      board_score: board_score,
      good: good_score
    }).to_json)
  end

  def inc_bad
    board_score = self.board.inc_score(-1)
    bad_score = self.inc(:bad, 1)
    self.board.notify(({
      type: :change_bad,
      comment_id: self.id,
      board_score: board_score,
      bad: bad_score
    }).to_json)
  end

  def add_image image
    self.images << image
    self.board.add_image image
    self.board.notify(({
      type: :add_comment_image,
      comment_id: self.id,
      html: image.to_thumb_html
    }).to_json)
  end

  def add_website website
    self.websites << website
    self.board.add_website website
    self.board.notify(({
      type: :add_comment_website,
      comment_id: self.id,
      html: website.to_thumb_html
    }).to_json)
  end

  def add_movie movie
    self.movies << movie
    self.board.add_movie movie
    self.board.notify(({
      type: :add_comment_movie,
      comment_id: self.id,
      html: movie.to_thumb_html
    }).to_json)
  end

  def check_and_add_image url
    image = Image.where(url: url).first
    if image
      self.add_image image
      true
    else
      false
    end
  end

  def check_and_add_website url
    website = Website.where(url: url).first
    if website
      self.add_website website
      true
    else
      false
    end
  end

  def check_and_add_movie url
    movie = Movie.where(url: url).first
    if movie
      self.add_movie movie
      true
    else
      false
    end
  end

  def notify_change_tags html 
    self.board.notify ({
      type: :change_comment_tags ,
      comment_id: self.id,
      html: html}).to_json
  end

  private
  def check_hash_id
    self.errors.add(:base, "なんらかのエラーが発生しました。") if self.hash_id.nil?
  end

  def check_remote_host
    # TODO check black list
    self.errors.add(:base, "書き込み制限されています。") if self.remote_host.nil? || self.user_agent.nil?
  end

end
