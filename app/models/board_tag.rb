# -*- coding: utf-8 -*-
class BoardTag
  include Mongoid::Document
  include Mongoid::Timestamps
  include TagNotifiable

  field :name, type: String
  field :used_count, type:Integer, default: 0

  # relations
  has_and_belongs_to_many :boards

  # indexes
  index({ name: 1 }, { unique: true, name: "name_index" })

  # 最新３個のデータ
  def popular_boards
    self.boards.desc(:score).limit(3)
  end

  #タグ側のサムネイル
  def thumbnail
    if (board = self.boards.first)
      board.thumbnail
    else
      'placeholder.png'
    end
  end

  def notify_new_comment html
    self.notify(({
      type: :new_comment,
      html: html
    }).to_json)
  end

  def notify_new_board html
    self.notify(({
      type: :new_board,
      html: html
    }).to_json)
  end

  def notify_add_board_tag html
    self.notify(({
      type: :add_board_tag,
      html: html
    }).to_json)
  end
end
