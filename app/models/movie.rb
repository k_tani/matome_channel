class Movie
  include Mongoid::Document
  include Mongoid::Timestamps
  include Storable

  storable_on :animation, :thumb

  # fields
  field :url, type: String
  field :title, type: String
  field :media, type: String

  # relation
  has_and_belongs_to_many :comments

  # indexes
  index({ url: 1 }, { name: "url_index" })

  def object_tag(options={})
    width  = options[:width] || 520
    height = options[:height] || 360
    html5  = options[:html5]
    case media
    when "youtube"
      tag =<<-HTML
        <object width="#{width}" height="#{height}">
          <param name="movie" value="#{embed_url}"></param>
          <param name="wmode" value="transparent"></param>
          <embed src="#{embed_url}" type="application/x-shockwave-flash"
            wmode="transparent" width="#{width}" height="#{height}"></embed>
        </object>
      HTML
    when "nicovideo"
      tag =<<-HTML
        <script type="text/javascript" src="#{embed_url}?w=#{width}&h=#{height}"></script>
        <noscript><a href="#{url}">#{title}</a></noscript>
      HTML
    end
  end

  def embed_url
    case media
    when "youtube"
      url.sub('watch?', '').sub('=', '/').sub('feature/', 'feature=')
    when "nicovideo"
      nicoid=url.match(/sm[0-9]+/)[0]
      "http://ext.nicovideo.jp/thumb_watch/#{nicoid}"
    end
  end

  def to_thumb_html
    get_view.render(
      :partial => "movies/thumb",
      :locals => {:movie => self})
  end

  def to_html
    get_view.render(
      :partial => "movies/movie",
      :locals => {:movie => self})
  end

  def hash_ids
    self.comments.map(&:hash_id).uniq
  end

  private
  def get_view
    view = ActionView::Base.new(Rails.configuration.paths["app/views"].first)
    class << view
      include MoviesHelper
      include Rails.application.routes.url_helpers
    end
    view
  end

end
