class BoardObserver
  include Mongoid::Document

  has_and_belongs_to_many :boards

end
