class CommentTag
  include Mongoid::Document
  include Mongoid::Timestamps
  include TagNotifiable

  field :name, type: String
  field :used_count, type:Integer, default: 0

  # relations
  has_and_belongs_to_many :comments

  # indexes
  index({ name: 1 }, { unique: true, name: "name_index" })

  def notify_new_comment html
    self.notify(({
      type: :new_comment,
      html: html 
    }).to_json)
  end

  def notify_add_tag html
    self.notify(({
      type: :add_comment_tag,
      html: html 
    }).to_json)
  end

end
