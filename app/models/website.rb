class Website
  include Mongoid::Document
  include Mongoid::Timestamps
  include Storable

  storable_on :thumb

  # fields
  field :url, type: String
  field :title, type: String

  # relation
  has_and_belongs_to_many :comments

  # indexes
  index({ url: 1 }, { name: "url_index" })

  def to_thumb_html
    get_view.render(
      :partial => "websites/thumb",
      :locals => {:website => self})
  end

  def to_html
    get_view.render(
      :partial => "websites/website",
      :locals => {:website => self})
  end

  def hash_ids
    self.comments.map(&:hash_id).uniq
  end

  private
  def get_view
    view = ActionView::Base.new(Rails.configuration.paths["app/views"].first)
    class << view
      include WebsitesHelper
      include Rails.application.routes.url_helpers
    end
    view
  end
end
