module CommentsHelper

  def html_body comment
    # urls
    urls = URI.extract(comment.body).
      select{|u| u =~ /^(ttp:\/\/|ttps:\/\/|http:\/\/|https:\/\/)/ }.
      sort{|a,b|b.length <=> a.length}.uniq
    html = if urls.any?
      comment.body.gsub(/#{urls.map{|u|Regexp.escape(u)}.join("|")}/) do |m|
        url = (m =~ /^(ttp:\/\/|ttps:\/\/)/) ? "h#{m}" : m
        "<a href=\"#{url}\" target=\"_brank\">#{m}</a>"
      end
    else
      comment.body
    end

    # res num anchor
    html = html.gsub(/(>>([0-9]+(\-[0-9]+){0,1}))/) do |m|
      comment_nums = $2.split('-')
      comment_nums = (comment_nums[0].to_i..comment_nums[1].to_i).to_a if comment_nums.length > 1
      comments = Comment.where(board_id: comment.board_id, :num.in => comment_nums)
      if comments.any?
        "<a href=\"#\" class=\"comment-anchor\" data-target-ids=\"#{comments.map(&:id).join(",")}\">&gt;&gt;#{$2}</a>"
      else
        m
      end
    end

    # ID anchor
    html.gsub!(/ID: *([0-9a-zA-Z+\/=]{#{HashIdMap::HASH_LENGTH}})/) do |m|
      comments = Comment.where(board_id: comment.board_id, hash_id: $1)
      if comments.any?
        "<a href=\"#\" class=\"comment-anchor\" data-target-ids=\"#{comments.map(&:id).join(",")}\">#{m}</a>"
      else
        m
      end
    end

    # new line
    html.gsub! /\r\n|\n/m, "<br/>"
    # quote
    loop do
      matched = false
      html.gsub! /(<br\/>)*<<<quote(<br\/>)*(.+)(<br\/>)*quote>>>(<br\/>)*/ do |m|
        matched = true
        "<div class='well well-small popover bottom quote'><div class='arrow'></div><p>#{$3}</p></div><br class='clearfix'/>"
      end
      break unless matched
    end
    raw(html)
  end

  def hash_id_html comment, hash_id_map, valid_control=true
    return comment.hash_id if hash_id_map.nil? || !valid_control
    hash_id = comment.hash_id
    comment_ids = hash_id_map[hash_id] || []
    link = link_to "#{hash_id}(#{comment_ids.index(comment.id).to_i+1}/#{comment_ids.count})", "#",
      :class => "comment-hash-id#{comment_ids.count >= 5 ? " hissi-id" : ""}",
      "data-target-ids" => comment_ids.join(",")
    raw(link)
  end

  def comment_tags_html tags
     html = ""
     tags.to_a.each do |t|
       html << "<div class='btn-group pull-left'>"
       html << button_tag( "#", class: "btn btn-inverse btn-mini") do
         raw("<i class='icon-tag icon-white'></i> <strong>#{t.name}</strong>")
       end
       html << button_tag( "#", class: "btn btn-inverse btn-mini comment-tag-remove", "data-id" => t.id ) do
         raw("<i class='icon-remove icon-white'></i>")
       end
       html << "</div>"
     end
     raw(html)
  end
end
