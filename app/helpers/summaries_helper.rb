module SummariesHelper

  def decoration_to_style decoration
    css =<<-CSS
      font-size: #{decoration.size}px; 
      line-height: #{decoration.size}px; 
      color: #{decoration.color};
      font-style: #{decoration.italic.to_s == "true" ? "italic" : "normal"};
      font-weight: #{decoration.bold.to_s == "true" ? "700" : "400"};
    CSS
  end

end
