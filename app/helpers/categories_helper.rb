module CategoriesHelper
  def board_category_navi category, self_link = false
    category_navi(category, self_link)do |type, c|
      if type == :all
        boards_path
      else
        category_boards_path(c)
      end
    end
  end

  def summary_category_navi category, self_link = false
    category_navi(category, self_link) do |type, c|
      if type == :all
        summaries_path
      else
        category_summaries_path(c)
      end
    end
  end

  def tag_list tags, category=nil
    html = ""
    len = 0
    skip = false
    tags.each do |t|
      if t.name.mlength + len > 32
        skip = true
        next
      end
      case t
      when BoardTag
        if category.nil?
          html << link_to( tag_boards_path(t), class: "btn btn-inverse btn-mini") do
            raw("<i class='icon-tag icon-white'></i> <strong>#{t.name}</strong>")
          end
        else
          html << link_to( tag_category_boards_path(id: category.id, tag_id: t.id), class: "btn btn-inverse btn-mini") do
            raw("<i class='icon-tag icon-white'></i> <strong>#{t.name}</strong>")
          end
        end
      when SummaryTag
        if category.nil?
          html << link_to( tag_summaries_path(t), class: "btn btn-inverse btn-mini") do
            raw("<i class='icon-tag icon-white'></i> <strong>#{t.name}</strong>")
          end
        else
          html << link_to( tag_category_summaries_path(id: category.id, tag_id: t.id), class: "btn btn-inverse btn-mini") do
            raw("<i class='icon-tag icon-white'></i> <strong>#{t.name}</strong>")
          end
        end
      end
      len += (t.name.mlength + 3)
    end
    html << "…他" if skip
    raw(html)
  end

  private
  def category_navi category, self_link=false
    html = "<div>"
    if category.nil?
      html << (self_link ? link_to("総合", yield(:all) ) : "総合")
    else
      c = category
      links = []
      2.times do |i|
        links << if i == 0
          self_link ? link_to(c.name, yield(:category, c)) : c.name
        else
          link_to(c.name, yield(:category, c))
        end
        c = c.parent
        break if c.nil?
      end
      links << link_to("総合", yield(:all) )
      html << links.reverse.join("&nbsp;&raquo;&nbsp;")
    end
    html << "</div>"
    raw(html)
  end
end
