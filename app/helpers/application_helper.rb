module ApplicationHelper

  def thumbnail_path target
    case target
    when Website
      thumb_website_path target
    when Image
      thumb_image_path target
    when Movie
      animation_movie_path target
    else
      target
    end
  end

end
