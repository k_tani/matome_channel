# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

class AddConditionDialog

  constructor: (@dialog) ->
    @attrs =
      good:
        name: "ｲｲﾈ!"
        conditions:
          gte:
            element: @dialog.find("#good-gte")
            relation: "以上"
            validate: "number"
            type: "text"
          lte:
            element: @dialog.find("#good-lte")
            relation: "以下"
            validate: "number"
            type: "text"
      bad:
        name: "ｲｸﾅｲ!"
        conditions:
          gte:
            element: @dialog.find("#bad-gte")
            relation: "以上"
            validate: "number"
            type: "text"
          lte:
            element: @dialog.find("#bad-lte")
            relation: "以下"
            validate: "number"
            type: "text"
      tag:
        name: "タグ"
        conditions:
          in:
            element: @dialog.find("#tag-in")
            relation: "を含む"
            type: "select"
      hash_id:
        name: "ID"
        conditions:
          in:
            element: @dialog.find("#hash_id-in")
            relation: "を含む"
            type: "select"
      created_at:
        name: "投稿日時"
        conditions:
          gte:
            element: @dialog.find("#created_at-gte")
            relation: "以降"
            validate: "datetime"
            type: "text"
          lte:
            element: @dialog.find("#created_at-lte")
            relation: "以前"
            validate: "datetime"
            type: "text"

    @cancel      = @dialog.find(".add-condition-cancel")
    @submit      = @dialog.find(".add-condition-submit")
    @cancel.click =>
      @hide()
    @submit.click =>
      @addCondition()

  addCondition: ->
    @validate()
    return false if @errors.length > 0
    num = $("#summary-form .condition-list .condition").length + 1
    html = @buildHtml(num)
    if html.length > 0
      $("#summary-form .nav-tabs").append "<li><a href='#condition-#{num}'>条件#{num}<b class='close'>&times;</b></a></li>"
      $("#summary-form .condition-list").append html
      $("#summary-form .nav-tabs a:last").tab("show")
      @clear()
    @hide()

  removeCondition: (close) ->
    header = close.parent().parent()
    content = $("#"+close.parent().attr("href").replace("#",""))
    header.remove()
    content.remove()
    $("#summary-form .nav-tabs li a").each (i,e) ->
      $(this).html("条件#{i+1}<b class='close'>&times;</b>")
      $(this).attr("href", "#condition-#{i+1}")
    $("#summary-form .condition-list .condition").each (i,e) ->
      $(this).attr("id", "condition-#{i+1}")
      $(this).find("input").each () ->
        name = $(this).attr("name")
        name = name.replace /\[[0-9]+\]/, "[#{i}]"
        $(this).attr("name", name)

  validate: ->
    @errors = []
    for k,attr of @attrs
      for rkey, cond of attr.conditions
        switch cond.validate
          when "number"
            @validate_number cond.element, attr.name
          when "datetime"
            @validate_datetime cond.element, attr.name

  validate_number: (element, attr_name) ->
    value = element.val()
    if value.length > 0
      num = parseInt(value, 10)
      if isNaN(num)
        @errors.push "#{attr_name}は数値で入力してください。"
        element.parents(".control-group").addClass("error")

  validate_datetime: (element, attr_name) ->
    value = element.val()
    if value.length > 0
      unless value.match /[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/
        @errors.push "#{attr_name}はYYYY-MM-DD hh:mm:ssの形式で入力してください。"
        element.parents(".control-group").addClass("error")


  buildHtml: (num) ->
    html = "<div id='condition-#{num}' class='condition tab-pane'>"
    cond_count = 0
    for k,attr of @attrs
      for rkey, cond of attr.conditions
        value = null
        if cond.type == "select"
          value = cond.element.select2("data").map (r) -> r.id
        else
          value = cond.element.val()
        if value && value.length > 0
          value = value.join(",") if $.isArray value
          html +=  "<div class='field control-group clearfix'>"
          html += "<div class='input-prepend input-append'><span class='add-on'>#{attr.name}</span>"
          html += "<input type='text' name='summary[conditions][#{num-1}][#{k}_#{rkey}]' value='#{value}' readonly='readonly'/>"
          html += "<span class='add-on relation'>#{cond.relation}</span></div></div>"
          cond_count += 1
    html += "</div>"
    html = "" if cond_count == 0
    html

  clear: ->
    for k,attr of @attrs
      for rkey, cond of attr.conditions
        if cond.type == "select"
          cond.element.select2("val", null)
        else
          cond.element.val("")

  show: ->
    @dialog.modal "show"
    false

  hide: ->
    @dialog.modal "hide"
    false

$ ->
  d = new AddConditionDialog $("#summary-form .add-condition-dialog")

  $("#summary-form .add-cond-btn").click ->
    d.show()

  $("#summary-form .reload-btn").click ->
    return false if $("#summary-form .nav-tabs li").length == 0
    params = $("#post-form").serializeArray()
    post_params = []
    for param in params
      post_params.push(param) unless param.name.match(/(summary\[items\]|_method)/)
    $.post "/summaries/items", post_params, (html) ->
      html = $(html).hide()
      $("#preview").html(html)
      html.fadeIn()
    false

  $("#summary-form .reload-add-btn").click ->
    return false if $("#summary-form .nav-tabs li").length == 0
    params = $("#post-form").serializeArray()
    post_params = []
    for param in params
      post_params.push(param) unless param.name.match(/(summary\[items\]|_method)/)
    $.post "/summaries/items", post_params, (html) ->
      $(html).find(".comment").each (i,nc) ->
        exists = $(".comment")
        return true if $("##{$(nc).attr("id")}").length > 0
        nn = parseInt($(nc).find(".comment-num").text(),10)
        return true if isNaN(nn)
        added = false
        pn = 0
        nc_cm = $(nc).next()
        exists.each (j,cc) ->
          cn = parseInt($(cc).find(".comment-num").text(),10)
          if !isNaN(cn) && nn < cn && nn > pn
            prev = $(cc).prev()
            if prev.length > 0
              $(cc).before nc
              $(cc).before nc_cm
            else
              $(cc).before nc
              $(cc).before nc_cm
            added = true
            return false
          pn = cn
        unless added
          $("#comment-list").append nc
          $("#comment-list").append nc_cm

    false

  $(document).on 'click', "#summary-form .nav-tabs li a .close", () ->
    $("#confirm-container").confirm
      title: "条件削除"
      body: "条件を削除します。よろしいですか？"
      callback: () =>
        d.removeCondition $(this)

  $(document).on "click", ".comment .comment-res", () ->
    cc = $(this).parents(".comment").next()
    if $(this).hasClass("active")
      $(this).removeClass("active")
      cc.find("textarea").val("")
      cc.hide()
    else
      $(this).addClass("active")
      cc.show()
    false

  $(document).on "click", ".comment .remove-res", () ->
    comment = $(this).parents(".comment")
    nx = comment.next()
    $("#confirm-container").confirm
      title: "コメント除外"
      body: "このコメントを除外します。よろしいですか？"
      callback: () =>
        nx.remove() if nx.hasClass("popover")
        comment.remove()
    false

  $(document).on "click", ".comment .comment-color", () ->
    body = $(this).parents(".comment").find(".comment-body")
    $(this).colorpicker('show').on 'changeColor', (e) =>
      color = e.color.toHex()
      $(this).find("div").css "background-color", color
      $(this).find("input").val color
      body.css "color", color

    false

  $(document).on "change", ".comment .comment-fontsize", () ->
    body = $(this).parents(".comment").find(".comment-body")
    size = "#{$(this).val()}px"
    body.css("font-size", size)
    body.css("line-height", size)
    false

  $(document).on "click", ".comment .comment-bold", () ->
    body = $(this).parents(".comment").find(".comment-body")
    if $(this).hasClass("active")
      $(this).removeClass("active")
      $(this).find("input").val false
      body.css("font-weight", "400")
    else
      $(this).addClass("active")
      $(this).find("input").val true
      body.css("font-weight", "700")
    false

  $(document).on "click", ".comment .comment-italic", () ->
    body = $(this).parents(".comment").find(".comment-body")
    if $(this).hasClass("active")
      $(this).find("input").val false
      $(this).removeClass("active")
      body.css("font-style", "normal")
    else
      $(this).addClass("active")
      $(this).find("input").val true
      body.css("font-style", "italic")
    false
  $("input[type=hidden][name='summary[publish]']").val( $("#publish-switch").bootstrapSwitch("status"))
  $("#publish-switch").on 'switch-change', (e, data) ->
    $(this).find("input[type=hidden]").val(data.value)

  $(".summary-description a.disp-desc").click ->
    $(this).parent().hide()
    $(".summary-all-description").fadeIn()
    false

  $('.summary-favorite').click () ->
    params = {}
    params[$('meta[name=csrf-param]').attr("content")] = $('meta[name=csrf-token]').attr("content")
    if $(this).hasClass("active")
      params["_method"] = "delete"
      $.post "/favorites/summaries/#{$(this).attr('data-id')}", params, (json) =>
        $(this).removeClass("active").attr('data-id', json.id).parent().show()
      , "json"
    else
      params["summary_id"] = $("#summary_id").val()
      $.post "/favorites/summaries", params, (json) =>
        $(this).addClass("active").attr('data-id', json.id).parent().show()
      , "json"
    false


