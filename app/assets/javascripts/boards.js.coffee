# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

class AddBoardTagDialog

  constructor: (@dialog) ->
    @tag_list = @dialog.find("#tag_list")
    @board_id = $("#board_id")
    @dialog.find(".add-tag-cancel").click () =>
      @hide()
    @dialog.find(".add-tag-post").click () =>
      @post()
    @dialog.on 'hide', () =>
      @clear()

  show: () ->
    @dialog.modal "show"
    false

  hide: () ->
    @dialog.modal "hide"
    false

  post: () ->
    params = {}
    params["tag_list"] = @tag_list.val()
    params[$('meta[name=csrf-param]').attr("content")] = $('meta[name=csrf-token]').attr("content")
    $.post "/boards/#{@board_id.val()}/tag", params, (json) =>
      @hide()
    , "json"
    false

  setTagList: (text) ->
    @tag_list.val(text)

  clear: () ->
    @setTagList("")

$ () ->
  $(document).on 'click', '#board-tab > li > a', ->
    $(this).tab 'show'
    $(this).find("span").remove()
    if $(this).attr("href") == "#websites-container"
      $("#website-list").masonry("reload")
    else if $(this).attr("href") == "#images-container"
      $("#image-list").masonry("reload")
    else if $(this).attr("href") == "#movies-container"
      $("#movie-list").masonry("reload")
    false

  $("#website-list").imagesLoaded ()->
    $("#website-list").masonry
      itemSelector: ".website"
  $("#image-list").imagesLoaded ()->
    $("#image-list").masonry
      itemSelector: ".image"
    $("#image-list a.thumbnail").colorbox
      rel:'a.thumbnail'
      maxWidth: "1200px"
      maxHEight: "600px"
  $("#movie-list").imagesLoaded ()->
    $("#movie-list").masonry
      itemSelector: ".movie"

  $(document).on "click", '#comment-pagination a:first', () ->
    $.get $(this).attr("href"), (html) ->
      dom = $(html)
      comments = dom.find(".comment").hide()
      pager = dom.find("#comment-pagination")
      comments.appendTo('#comment-list').fadeIn()
      if pager.length > 0
        $('#comment-pagination').replaceWith(pager)
      else
        $('#comment-pagination').remove()
    false
  $(document).on "click", '#website-pagination a:first', () ->
    $.get $(this).attr("href"), (html) ->
      dom = $(html)
      websites = dom.find(".website").hide()
      pager = dom.find("#website-pagination")
      websites.appendTo('#website-list').fadeIn()
      $("#website-list").imagesLoaded ()->
        $("#website-list").masonry("appended", websites)
      if pager.length > 0
        $('#website-pagination').replaceWith(pager)
      else
        $('#website-pagination').remove()
    false
  $(document).on "click", '#image-pagination a:first', () ->
    $.get $(this).attr("href"), (html) ->
      dom = $(html)
      images = dom.find(".image").hide()
      pager = dom.find("#image-pagination")
      images.appendTo('#image-list').fadeIn()
      $("#image-list").imagesLoaded ()->
        $("#image-list").masonry("appended", images)
        $("#image-list a.thumbnail").colorbox
          rel:'a.thumbnail'
          maxWidth: "1200px"
          maxHEight: "600px"
      if pager.length > 0
        $('#image-pagination').replaceWith(pager)
      else
        $('#image-pagination').remove()
    false

  $(document).on "click", '#movie-pagination a:first', () ->
    $.get $(this).attr("href"), (html) ->
      dom = $(html)
      movies = dom.find(".movie").hide()
      pager = dom.find("#movie-pagination")
      movies.appendTo('#movie-list').fadeIn()
      $("#movie-list").imagesLoaded ()->
        $("#movie-list").masonry("appended", movies)
      if pager.length > 0
        $('#movie-pagination').replaceWith(pager)
      else
        $('#movie-pagination').remove()
    false

  $('.regist-favorite').click () ->
    $(this).parents('.dropdown-menu').prev().dropdown('toggle')
    params = {}
    params["board_id"] = $("#board_id").val()
    params[$('meta[name=csrf-param]').attr("content")] = $('meta[name=csrf-token]').attr("content")
    $.post "/favorites/boards", params, (json) ->
      $('.regist-favorite').parent().hide()
      $('.unregist-favorite').attr('data-id', json.id).parent().show()
    , "json"
    false

  $('.unregist-favorite').click () ->
    $(this).parents('.dropdown-menu').prev().dropdown('toggle')
    params = {}
    params["_method"] = "delete"
    params["board_id"] = $("#board_id").val()
    params[$('meta[name=csrf-param]').attr("content")] = $('meta[name=csrf-token]').attr("content")
    $.post "/favorites/boards/#{$(this).attr('data-id')}", params, (json) ->
      $('.regist-favorite').parent().show()
      $('.unregist-favorite').parent().hide()
    , "json"
    false

  at = new AddBoardTagDialog $("#add-board-tag-dialog")
  $(document).on 'click', '.add-board-tag', () ->
    at.show()
    false

  $(document).on 'click', '.board-tag-remove', () ->
    bid = $("#board_id").val()
    params = {}
    tag = $(this).parent()
    params["tag_id"] = $(this).attr("data-id")
    params["_method"] = "delete"
    params[$('meta[name=csrf-param]').attr("content")] = $('meta[name=csrf-token]').attr("content")
    $("#confirm-container").confirm
      title: "タグ削除"
      body: "タグ「#{$(this).prev().text().trim()}」を削除します。よろしいですか？"
      callback: () ->
        $.post("/boards/#{bid}/tag", params, (json) =>
          json
        , "json")
        false
    false

