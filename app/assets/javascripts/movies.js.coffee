# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
class MoviePlayerDialog

  constructor: (@dialog) ->
    @title = @dialog.find(".modal-header h3")
    @movie = @dialog.find(".modal-body")
    @dialog.on 'hide', () =>
      @clear()

  show: () ->
    @dialog.modal "show"

  hide: () ->
    @dialog.modal "hide"

  setTitle: (title) ->
    @title.text(title)

  setMovie: (movie) ->
    @movie.html(movie)

  clear: () ->
    @setMovie ""

$ ->
  dialog = new MoviePlayerDialog $('.movie-player-dialog')

  $(document).on 'click', '.movie-link', () ->
    dialog.setTitle $(this).parents('.movie').find('h4 a').text()
    mid =  $(this).parents('.movie').attr("data-id")
    movie =  $(this).parents('.movie').find(".movie-object").html()
    dialog.setMovie movie
    dialog.show()
    false

