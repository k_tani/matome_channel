# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
# WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
# GO AFTER THE REQUIRES BELOW.
#
#= require jquery
#= require jquery_ujs
#= require jquery.masonry.min
#= require_tree .
#= require bootstrap.min
#= require bootstrap-datetimepicker
#= require bootstrap-datetimepicker.ja
#= require bootstrap-colorpicker
#= require bootstrapSwitch
#= require select2
#= require jquery.colorbox-min
#= require jquery.ui.widget
#= require canvas-to-blob.min
#= require load-image.min
#= require jquery.iframe-transport
#= require jquery.fileupload
#= require jquery.fileupload-process
#= require jquery.fileupload-resize
#= require jquery.fileupload-validate
#= require jquery.fileupload-ui

(($) ->
  $.fn.extend
    message:  (options) ->
      html = """
        <div class="modal" id="messageContainer">
          <div class="modal-header"><a class="close" data-dismiss="modal">×</a>
            <h3>#Heading#</h3>
          </div>
          <div class="modal-body">
            #Body#
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-primary okBtn">#OK_BUTTON#</a>
          </div>
        </div>
      """
      defaults =
        title: 'インフォメーション',
        body:'',
        ok: 'OK'
      options = $.extend defaults, options
      html = html.replace('#Heading#',options.title).
                  replace('#Body#',options.body).
                  replace('#OK_BUTTON#',options.ok)
      $(this).html html
      $(this).modal 'show'
      context = $(this)
      $('#messageContainer .okBtn',this).click () ->
        $(context).modal('hide')
        false

    confirm:  (options) ->
      html = """
        <div class="modal" id="confirmContainer">
          <div class="modal-header"><a class="close" data-dismiss="modal">×</a>
            <h3>#Heading#</h3>
          </div>
          <div class="modal-body">
            #Body#
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-primary confirmYesBtn">#OK_BUTTON#</a>
            <a href="#" class="btn" data-dismiss="modal">#CANCEL_BUTTON#</a>
          </div>
        </div>
      """
      defaults =
        title: '確認',
        body:'よろしいですか？',
        ok: 'はい'
        cancel: 'いいえ'
        callback : null
      options = $.extend defaults, options
      html = html.replace('#Heading#',options.title).
                  replace('#Body#',options.body).
                  replace('#OK_BUTTON#',options.ok).
                  replace('#CANCEL_BUTTON#',options.cancel)
      $(this).html html
      $(this).modal 'show'
      context = $(this)
      $('#confirmContainer .confirmYesBtn',this).click () ->
        options.callback() if options.callback
        $(context).modal('hide')
        false
)(jQuery)

$ ->
  $(".datetimepicker").datetimepicker
    language: "ja"
    format: 'yyyy-MM-dd hh:mm:ss'

  $(".multiselectable").select2
    width: "element"

  topBtn = $('#page-top').hide().click () ->
    $('body,html').animate
      scrollTop: 0
    , 500
    $(this).blur()
    false
  $(window).scroll ->
    if $(this).scrollTop() != 0
      topBtn.fadeIn()
    else
      topBtn.fadeOut()

