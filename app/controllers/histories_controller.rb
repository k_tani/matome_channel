class HistoriesController < ApplicationController

  before_filter :authenticate_user!

  def my_boards
    @boards = current_user.boards.order_by(:created_at.desc).page(params[:page]).per(20)
  end

  def my_comments
    @comments = current_user.comments.order_by(:created_at.desc).page(params[:page]).per(20)
  end

  def my_summaries
    @summaries = current_user.summaries.order_by(:created_at.desc).page(params[:page]).per(20)
  end

  def boards
    @boards = current_user.refer_history.boards.page(params[:page]).per(20)
  end

  def summaries
    @summaries = current_user.refer_history.summaries.page(params[:page]).per(20)
  end

end
