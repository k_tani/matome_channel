class MoviesController < ApplicationController

  def animation
    movie = Movie.find(params[:id])
    if Settings.mogilefs.nginx_proxy
      response.header["Content-Type"] = "image/gif"
      response.header["X-Accel-Redirect"] = "/reproxy"
      response.header["x-reproxy-url"] = movie.animation_paths.sample
      render nothing: true
    else
      send_data movie.animation, :type => "image/gif", :disposition => 'inline'
    end
  end

  def thumb
    movie = Movie.find(params[:id])
    if Settings.mogilefs.nginx_proxy
      response.header["Content-Type"] = "image/jpeg"
      response.header["X-Accel-Redirect"] = "/reproxy"
      response.header["x-reproxy-url"] = movie.thumb_paths.sample
      render nothing: true
    else
      send_data movie.thumb,:type => "image/jpeg", :disposition => 'inline'
    end
  end

end
