class BoardsController < ApplicationController

  def index
    @categories = Category.roots
    if params[:id].nil?
      @boards = Board.order_by(score: :desc, created_at: :desc).page(params[:page]).per(15)
      @new_board_path = new_board_path
    else
      @category = Category.find params[:id]
      @boards = if @category.root?
        category_ids = @category.children.map{|c|c.id}
        category_ids.push @category.id
        Board.order_by(score: :desc, created_at: :desc).
              where(:category_id.in => category_ids).
              page(params[:page]).per(15)
      else
        Board.order_by(score: :desc, created_at: :desc).
              where(:category_id => @category.id).
              page(params[:page]).per(15)
      end
      @new_board_path = new_category_board_path(@category)
    end
  end

  def tag
    @tag = BoardTag.find params[:tag_id]
    index
    @boards = @boards.tagged_with(@tag.name)
    render :index
  end


  def new
    @board = Board.new
    @board.category = Category.find params[:id] unless params[:id].blank?
    @board.comments << Comment.new
    @board.tags = []
    @board.build_config
  end

  def show
    @board = Board.find params[:id]
    current_user.refer_history.push @board
    @comments = @board.comments.order_by(created_at: :desc).limit(Comment::COUNT_PER_PAGE).to_a
    @hash_id_map = HashIdMap.get_map @comments.map(&:hash_id).uniq, @board.id
    @images = @board.board_images.order_by(count: :desc, created_at: :desc).
                includes(:image).limit(BoardImage::COUNT_PER_PAGE).to_a
    @websites = @board.board_websites.order_by(count: :desc, created_at: :desc).
                includes(:website).limit(BoardWebsite::COUNT_PER_PAGE).to_a
    @movies = @board.board_movies.order_by(count: :desc, created_at: :desc).
                includes(:movie).limit(BoardMovie::COUNT_PER_PAGE).to_a
  end

  def create
    @board = Board.new(params[:board])
    @board.user_id = current_user.id
    @board.category = Category.find(params[:board][:category_id]) if params[:board][:category_id].present?
    @board.configs_attributes = params[:board][:configs_attributes]
    @board.build_tags params[:board][:tag_list]
    comments_attrs = {}
    params[:board][:comments_attributes].each do |k,v|
      comments_attrs = v.to_h
      comments_attrs[:remote_host] = request.remote_ip
      comments_attrs[:user_agent]  = request.user_agent
    end
    comment = Comment.new comments_attrs do |c|
      c.user = current_user if user_signed_in?
    end
    @board.comments << comment

    respond_to do |format|
      if @board.save
        notify_html = render_to_string(partial: "tags/new_board", locals: { board: @board } ) 
        @board.tags.each do |tag|
          tag.notify_new_board notify_html
        end
        format.html { redirect_to @board, notice: 'board was successfully created.' }
        format.json { render json: @board, status: :created, location: @board }
      else
        @board.configs.each{|c|c.name = Board::CONFIGS[c.key.to_sym][:name]}
        format.html { render action: "new" }
        format.json { render json: @board.errors, status: :unprocessable_entity }
      end
    end
  end

  def comments
    @board = Board.find params[:id]
    num = params[:num]
    @comments = @board.comments.order_by(created_at: :desc).
                  limit(Comment::COUNT_PER_PAGE)
    if md = num.match(/^-([0-9]+)/)
      @comments = @comments.where(:num.lte => md[1])
    elsif md = num.match(/^([0-9]+)-/)
      @comments = @comments.where(:num.gte => md[1] )
    end
    @comments = @comments.to_a
    @hash_id_map = HashIdMap.get_map @comments.map(&:hash_id).uniq, @board.id
    if request.xhr?
      render partial: "comments/list", locals: {comments: @comments, hash_id_map: @hash_id_map}
    else
      @images = @board.board_images.order_by(count: :desc, created_at: :desc).
                  includes(:image).limit(BoardImage::COUNT_PER_PAGE).to_a
      @websites = @board.board_websites.order_by(count: :desc, created_at: :desc).
                  includes(:website).limit(BoardWebsite::COUNT_PER_PAGE).to_a
      @movies = @board.board_movies.order_by(count: :desc, created_at: :desc).
                  includes(:movie).limit(BoardMovie::COUNT_PER_PAGE).to_a
      render "boards/show"
    end
  end

  def websites
    @board = Board.find params[:id]
    num = params[:num]
    @websites = @board.board_websites.order_by(count: :desc, created_at: :desc).
                includes(:website).limit(BoardWebsite::COUNT_PER_PAGE)
    if md = num.match(/^-([0-9a-f]+)/)
      w = BoardWebsite.find(md[1])
      @websites = @websites.where(:count.lte => w.count, :created_at.lt => w.created_at)
    elsif md = num.match(/^([0-9a-f]+)-/)
      w = BoardWebsite.find(md[1])
      @websites = @websites.where(:count.gte => w.count, :created_at.gt => w.created_at)
    end
    render partial: "websites/list", locals: {websites: @websites.to_a}
  end

  def images
    @board = Board.find params[:id]
    num = params[:num]
    @images = @board.board_images.order_by(count: :desc, created_at: :desc).
                includes(:image).limit(BoardImage::COUNT_PER_PAGE)
    if md = num.match(/^-([0-9a-f]+)/)
      w = BoardImage.find(md[1])
      @images = @images.where(:count.lte => w.count, :created_at.lt => w.created_at)
    elsif md = num.match(/^([0-9a-f]+)-/)
      w = BoardImage.find(md[1])
      @images = @images.where(:count.gte => w.count, :created_at.gt => w.created_at)
    end
    render partial: "images/list", locals: {images: @images.to_a}
  end

  def movies
    @board = Board.find params[:id]
    num = params[:num]
    @movies = @board.board_movies.order_by(count: :desc, created_at: :desc).
                includes(:movie).limit(BoardMovie::COUNT_PER_PAGE)
    if md = num.match(/^-([0-9a-f]+)/)
      w = BoardMovie.find(md[1])
      @movies = @movies.where(:count.lte => w.count, :created_at.lt => w.created_at)
    elsif md = num.match(/^([0-9a-f]+)-/)
      w = BoardMovie.find(md[1])
      @movies = @movies.where(:count.gte => w.count, :created_at.gt => w.created_at)
    end
    render partial: "movies/list", locals: {movies: @movies.to_a}
  end

  def add_tag
    @board = Board.find(params[:id])
    @board.add_tags params[:tag_list]
    @board.tags.each do |t|
      t.notify_add_board_tag render_to_string(
        partial: "tags/add_board_tag",
        locals: { board: @board } 
      ) 
    end
    @board.notify_change_tags render_to_string(partial: "boards/tag", locals: {tags: @board.tags})
    render json: {result: :ok}
  end

  def remove_tag
    @board = Board.find(params[:id])
    tag = BoardTag.find(params[:tag_id])
    @board.remove_tags tag
    @board.reload
    @board.notify_change_tags render_to_string(partial: "boards/tag", locals: {tags: @board.tags})
    render json: {result: :ok}
  end
end

