class ImagesController < ApplicationController

  def create
    comment = Comment.find params[:comment_id]
    image = Image.new(params[:image]) do |i|
      i.content_type = i.upload_file.content_type
      i.image = i.upload_file.read
    end
    if image.save
      Resque.enqueue CreateThumbnail, comment.id, image.id
      render :json => {result: :ok}
    else
      render :json => {result: :ng, status: :unprocessable_entity}
    end
  end

  def image
    image = Image.find(params[:id])
    if Settings.mogilefs.nginx_proxy
      response.header["Content-Type"] = image.content_type
      response.header["X-Accel-Redirect"] = "/reproxy"
      response.header["x-reproxy-url"] = image.image_paths.sample
      render nothing: true
    else
      send_data image.image, :type => image.content_type, :disposition => 'inline'
    end
  end

  def thumb
    image = Image.find(params[:id])
    if Settings.mogilefs.nginx_proxy
      response.header["Content-Type"] = image.content_type
      response.header["X-Accel-Redirect"] = "/reproxy"
      response.header["x-reproxy-url"] = image.thumb_paths.sample
      render nothing: true
    else
      send_data image.thumb,:type => image.content_type, :disposition => 'inline'
    end
  end

end
