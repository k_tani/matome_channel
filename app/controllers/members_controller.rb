class MembersController < ApplicationController

  before_filter :get_user

  def board_favorites
    @favorites = @user.board_favorites.order_by(created_at: :desc).page(params[:page]).per(20)
  end

  def summary_favorites
    @favorites = @user.summary_favorites.order_by(created_at: :desc).page(params[:page]).per(20)
  end

  def boards
    @boards = @user.boards.order_by(created_at: :desc).page(params[:page]).per(20)
  end

  def summaries
    @summaries = @user.summaries.order_by(created_at: :desc).page(params[:page]).per(20)
  end


  private
  def get_user
    @user = User.find(params[:id])
  end

end
