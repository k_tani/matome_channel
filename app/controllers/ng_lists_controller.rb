class NgListsController < ApplicationController

  def show
    render json: current_user.get_ng_list
  end

end
