# -*- coding: utf-8 -*-
class TagsController < ApplicationController

  def board
    get_tags BoardTag
  end

  def summary
    get_tags SummaryTag
  end

  def observe
    @tag_name = params[:tag]
    @items = []
    if !@tag_name.blank? && !@tag_name.include?(",")
      @comment_tag = CommentTag.find_or_create_by name: params[:tag]
      @board_tag = BoardTag.find_or_create_by name: params[:tag]
      @summary_tag = SummaryTag.find_or_create_by name: params[:tag]
      @items = @comment_tag.comments.desc(:updated_at).limit(20).to_a
      @items += @board_tag.boards.desc(:updated_at).limit(20).to_a
      @items += @summary_tag.summaries.where(publish: true).desc(:updated_at).limit(20).to_a
      @items.sort!{|a,b|b.updated_at <=> a.updated_at}[0...20]
    elsif @tag_name.blank?
      @error = "タグが指定されていません"
    else
      @error = "タグは1つしか指定出来ません。"
    end
  end

  private

  def get_tags target
    @tags = target.where(:used_count.gt => 0)
    case params[:sort]
    when "use"
      @tags = @tags.order_by(used_count: :desc, created_at: :desc)
    else
      @tags = @tags.order_by(created_at: :desc)
    end
    @tags = @tags.page(params[:page]).per(50)
  end
end

