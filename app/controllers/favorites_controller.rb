class FavoritesController < ApplicationController

  before_filter :authenticate_user!

  def boards
    @user = current_user
    @favorites = current_user.board_favorites.order_by(created_at: :desc).page(params[:page]).per(20)
  end

  def summaries
    @user = current_user
    @favorites = current_user.summary_favorites.order_by(created_at: :desc).page(params[:page]).per(20)
  end

  def create_board
    board = Board.find params[:board_id]
    favorite = BoardFavorite.new{|f|f.board = board}
    current_user.board_favorites << favorite
    board.notify_change_favorites
    render :json => {id: favorite.id}
  end

  def create_summary
    summary = Summary.find params[:summary_id]
    favorite = SummaryFavorite.new{|f|f.summary = summary}
    current_user.summary_favorites << favorite
    render :json => {id: favorite.id}
  end

  def destroy_board
    favorite = BoardFavorite.find params[:id]
    board = favorite.board
    favorite.destroy
    board.reload.notify_change_favorites
    render :json => {result: :ok}
  end

  def destroy_summary
    favorite = SummaryFavorite.find params[:id]
    summary = favorite.summary
    favorite.destroy
    render :json => {result: :ok}
  end

end
