class CommentsController < ApplicationController

  def show
    @comment = Comment.find(params[:id])
    hash_id_map = HashIdMap.get_map  @comment.hash_id.to_a,  @comment.board_id
    valid_control = (params[:control] == "true" ? true : false)

    respond_to do |format|
      format.html do
        if request.xhr?
          mode = params[:mode]
          mode = "comments" unless ["comments", "summaries"].include?(mode)
          if mode == "comments"
            render partial: "comments/comment", locals: {comment: @comment, hash_id_map: hash_id_map, valid_control: valid_control}
          else
            deco = Struct.new(*(Summary::DefaultDecoration.keys)).new(*(Summary::DefaultDecoration.values))
            item = Struct.new(:comment, :decoration).new(@comment, deco)
            render partial: "summaries/comment", locals: {item: item, hash_id_map: hash_id_map}
          end
        else
          render :show
        end
      end
      format.json { render json: @comment.to_json(only: [:_id, :num, :name, :body, :hash_id, :created_at, :good, :bad]) }
    end
  end

  def create
    @comment = Comment.new(params[:comment])
    @comment.user_id = current_user.id
    @comment.remote_host = request.remote_ip
    @comment.user_agent  = request.user_agent
    @comment.build_tags(params[:comment][:tag_list]) if params[:comment][:tag_list]
    respond_to do |format|
      if @comment.save
        hash_id_map = HashIdMap.get_map  @comment.hash_id.to_a,  @comment.board_id
        notify_html = render_to_string(partial: "tags/new_comment", formats: "html", locals: {comment: @comment})
        btags = []
        @comment.board.tags.each do |t|
          t.notify_new_comment notify_html
          btags << t.name
        end
        @comment.tags.each do |t| 
          t.notify_new_comment notify_html unless btags.include?(t.name)
        end
        html = render_to_string partial: "comments/comment", formats: "html",locals: {comment: @comment, hash_id_map: hash_id_map, valid_control: true}
        @comment.notify_new_comment(html, hash_id_map[@comment.hash_id].count)
        format.html { redirect_to @comment, notice: 'comment was successfully created.' }
        format.json { render json: @comment.to_json(only: :_id), status: :created, location: @comment}
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors.to_json_local, status: :unprocessable_entity }
      end
    end
  end

  def good
    comment = Comment.find(params[:id])
    comment.inc_good
    current_user.add_check_list comment, 1
    render json: comment.to_json(only: :good)
  end

  def bad
    comment = Comment.find(params[:id])
    comment.inc_bad
    current_user.add_check_list comment, 0
    render json: comment.to_json(only: :bad)
  end

  def add_ng
    comment = Comment.find(params[:id])
    current_user.add_ng_list comment
    BlackHost.create target: comment.remote_host, reporter: request.remote_ip
    render json: {result: :ok}
  end

  def remove_ng
    comment = Comment.find(params[:id])
    current_user.remove_ng_list comment
    BlackHost.where(target: comment.remote_host, reporter: request.remote_ip).
      order_by(created_at: :desc).first.try(:destroy)
    render json: {result: :ok}
  end

  def add_tag
    comment = Comment.find(params[:id])
    hash_id_map = HashIdMap.get_map  comment.hash_id.to_a, comment.board_id
    notify_html =  render_to_string(partial: "tags/add_comment_tag", locals: {comment: comment}) 
    comment.add_tags(params[:tag_list]) do |tag|
       tag.notify_add_tag notify_html
    end
    comment.notify_change_tags render_to_string(partial: "comments/tag", locals: {tags: comment.tags})
    render json: {result: :ok}
  end

  def remove_tag
    comment = Comment.find(params[:id])
    tag = CommentTag.find(params[:tag_id])
    comment.remove_tags tag
    comment.reload
    comment.notify_change_tags render_to_string(partial: "comments/tag", locals: {tags: comment.tags})
    render json: {result: :ok}
  end

end

