class SummariesController < ApplicationController

  before_filter :authenticate_user!, only: [:new, :create, :edit, :update, :destroy, :mine]
  before_filter :require_mine!, only: [:edit, :update, :destroy]

  def index
    @categories = Category.roots
    if params[:id].nil?
      @summaries = Summary.order_by(created_at: :desc).where(publish: true).page(params[:page]).per(15)
    else
      @category = Category.find params[:id]
      @summaries = if @category.root?
        category_ids = @category.children.map{|c|c.id}
        category_ids.push @category.id
        Summary.order_by(created_at: :desc).where(publish: true).
              where(:category_id.in => category_ids).
              page(params[:page]).per(15)
      else
        Summary.order_by(created_at: :desc).where(publish: true).
              where(:category_id => @category.id).
              page(params[:page]).per(15)
      end
    end
  end

  def tag
    @tag = SummaryTag.find params[:tag_id]
    index
    @summaries = @summaries.tagged_with(@tag.name)
    render :index
  end

  def new
    @summary = Summary.new
    @summary.board = Board.find params[:board_id]
    @tags = CommentTag.where(:comment_ids.in => @summary.board.comments.map(&:id))
  end

  def edit
    @comments =  @summary.comments.to_a
    @hash_id_map = HashIdMap.get_map @comments.map(&:hash_id).uniq, @summary.board_id
    @tags = CommentTag.where(:comment_ids.in => @summary.board.comments.map(&:id))
  end

  def show
    @summary = Summary.find params[:id]
    redirect_to root_path if @summary.user != current_user && @summary.private?
    current_user.refer_history.push @summary
    @summary.inc :refered_count, 1 unless crawler?
    @comments =  @summary.comments.to_a
    @hash_id_map = HashIdMap.get_map @comments.map(&:hash_id).uniq, @summary.board_id
  end

  def create
    @summary = Summary.new params[:summary]
    @summary.category = @summary.board.category
    @summary.user = current_user
    @summary.items = @summary.items.sort{|a,b|a[0].to_i <=> b[0].to_i}.map{|k,v|v}
    @summary.build_tags params[:summary][:tag_list]
    respond_to do |format|
      if @summary.save
        if @summary.publish
          notify_html = render_to_string partial: "tags/new_summary", locals: {summary: @summary}
          @summary.tags.each do |tag|
            tag.notify_new_summary notify_html
          end
        end
        format.html { redirect_to @summary, notice: 'summary was successfully created.' }
        format.json { render json: @summary, status: :created, location: @summary }
      else
        @tags = CommentTag.where(:comment_ids.in => @summary.board.comments.map(&:id))
        format.html { render action: "new" }
        format.json { render json: @summary.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @summary.title = params[:summary][:title]
    @summary.description = params[:summary][:description]
    @summary.publish = params[:summary][:publish]
    @summary.items = params[:summary][:items].sort{|a,b|a[0].to_i <=> b[0].to_i}.map{|k,v|v}
    @summary.build_tags params[:summary][:tag_list]
    respond_to do |format|
      if @summary.save
        if @summary.publish
          notify_html = render_to_string partial: "tags/update_summary", locals: {summary: @summary}
          @summary.tags.each do |tag|
            tag.notify_update_summary notify_html
          end
        end
        format.html { redirect_to @summary, notice: 'summary was successfully created.' }
        format.json { render json: @summary, status: :created, location: @summary }
      else
        @comments =  @summary.comments.to_a
        @hash_id_map = HashIdMap.get_map @comments.map(&:hash_id).uniq, @summary.board_id
        @tags = CommentTag.where(:comment_ids.in => @summary.board.comments.map(&:id))
        format.html { render action: "edit" }
        format.json { render json: @summary.errors, status: :unprocessable_entity }
      end
    end
  end

  def items
    summary = Summary.new params[:summary]
    items = summary.get_items(params[:summary][:conditions]).to_a
    hash_id_map = HashIdMap.get_map items.map(&:comment).map(&:hash_id).uniq, summary.board.id
    render partial: 'edit_list', locals: {items: items, hash_id_map: hash_id_map}
  end

  private
  def require_mine!
    @summary = Summary.find params[:id]
    redirect_to root_path if @summary.user != current_user
  end
end
