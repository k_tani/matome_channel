class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from Mongoid::Errors::DocumentNotFound, :with => :error_404

  def current_user
    super || AnonymousUser.new(session)
  end

  def user_signed_in?
    current_user.instance_of? User
  end

  def crawler?
    request.user_agent =~ /(#{Settings.crawlers.join("|")})/
  end

  def error_404
    render :template => '/shared/error_404', :status => 404
  end

end
