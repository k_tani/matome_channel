class WebsitesController < ApplicationController

  def thumb
    website = Website.find(params[:id])
    if Settings.mogilefs.nginx_proxy
      response.header["Content-Type"] = "image/png"
      response.header["X-Accel-Redirect"] = "/reproxy"
      response.header["x-reproxy-url"] = website.thumb_paths.sample
      render nothing: true
    else
      send_data website.thumb, :type => "image/png", :disposition => 'inline'
    end
  end

end
