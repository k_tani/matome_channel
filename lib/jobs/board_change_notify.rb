class BoardChangeNotify
  DefaultQueue = :board_change_notify
  @queue = DefaultQueue 

  def self.namespace namespace
    @queue = "#{namespace}:#{DefaultQueue}"
    yield self
    @queue = DefaultQueue
  end

  def self.perform(observer_id, json)
    # Node.js側でやるので無処理
    # 将来的にem-websocket使うことになったら実装する。
  end
end

