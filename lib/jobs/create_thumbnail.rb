class CreateThumbnail
  @queue = :create_thumbnail

  def self.perform(comment_id, image_id)
    comment = Comment.find comment_id
    image = Image.find image_id
    ilist = Magick::ImageList.new(image.image_paths.sample)
    ilist.each do |i|
      i.resize_to_fit!(240,180)
    end
    image.thumb = ilist.to_blob
    comment.add_image image
  end
end

