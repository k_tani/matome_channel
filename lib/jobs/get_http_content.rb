class GetHttpContent
  @queue = :get_http_content

  def self.perform(comment_id, url)
    comment = Comment.find(comment_id)
    return if process_youtube comment, url
    return if process_niconico comment, url
    res = Faraday.new(url: url){|b|
      b.use FaradayMiddleware::FollowRedirects
      b.adapter :net_http
    }.get{|req|req.options[:timeout]=10 }
    return if res.status != 200
    if res.headers[:content_type] =~ /image\/(jpeg|jpg|gif|x-png|png)/i
      return if comment.check_and_add_image(url)
      image = Image.new do |i|
        i.url = url
        i.content_type = res.headers[:content_type]
        i.image = res.body
        ilist = Magick::ImageList.new
        Magick::Image.from_blob(res.body).each do |i|
          ilist.push i.resize_to_fit(240,180)
        end
        i.thumb = ilist.to_blob
      end
      comment.add_image image
    elsif res.headers[:content_type] =~ /application\/pdf/i
      website = Website.new do |w|
        w.title = url
        w.url = url
        w.thumb = Magick::Image.from_blob(res.body).first.resize_to_fit(240,180).to_blob
      end
      comment.add_website website
    else
      return if comment.check_and_add_website(url)
      website = Website.new do |w|
        w.title = Nokogiri::HTML(open(url).read.force_encoding("UTF-8").
          encode("UTF-16BE", "UTF-8", :invalid => :replace, :undef => :replace, :replace => '?').
          encode("UTF-8")).search("title").text rescue url
        w.title.strip!
        w.title = url if w.title.length == 0
        w.url = url
        tmp = "#{Rails.root}/tmp/#{w.id}_thumb.png"
        system("#{Settings.nodejs.path} #{Rails.root}/node/webshot.js \"#{url}\" \"#{tmp}\"")
        w.thumb = Magick::ImageList.new(tmp).resize_to_fit(240, 180).to_blob
        FileUtils.rm_f tmp
      end
      comment.add_website website
    end
  end

  def self.process_youtube comment, url
    return false unless url =~ /youtube/
    youtube = YouTubeIt::Client.new dev_key: Settings.youtube.devkey
    video = youtube.video_by url rescue nil
    return false if video.nil?
    return true if comment.check_and_add_movie video.player_url
    thumbs = {}
    video.thumbnails.each do |t|
      thumbs["#{t.width}x#{t.height}"] ||= []
      thumbs["#{t.width}x#{t.height}"] << t.url
    end
    movie = Movie.new do |m|
      m.media = "youtube"
      m.url = video.player_url
      m.title  = video.title
      images = Magick::ImageList.new(*thumbs["120x90"])
      images.each{|a|a.delay=100}
      tmp = "#{Rails.root}/tmp/#{m.id}_animation.gif"
      images.write tmp
      m.animation_file = tmp
      m.thumb = Magick::ImageList.new(*thumbs["480x360"]).resize_to_fit(240,180).to_blob
      FileUtils.rm_f tmp
    end
    comment.add_movie movie
    true
  end

  def self.process_niconico comment, url
    return false unless (md = url.match(/nicovideo\.jp\/.+(sm[0-9]+)/)) && md[1]
    doc = Nokogiri::XML(open("http://ext.nicovideo.jp/api/getthumbinfo/#{md[1]}")) rescue nil
    return false if doc.nil?
    return true if comment.check_and_add_movie doc.search("watch_url") 
    movie = Movie.new do |m|
      m.media = "nicovideo"
      m.url = doc.search("watch_url").text
      m.title  = doc.search("title").text
      thumb = Magick::ImageList.new(doc.search("thumbnail_url").text).resize_to_fit(240,180).to_blob
      m.animation = thumb
      m.thumb = thumb
    end
    comment.add_movie movie
    true
  end

end

