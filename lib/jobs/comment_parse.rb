class CommentParse
  @queue = :comment_parse

  def self.perform(comment_id, body)
    comment = Comment.find(comment_id)
    urls = URI.extract(body).select{|u| u =~ /^(ttp:\/\/|ttps:\/\/|http:\/\/|https:\/\/)/ }
    urls.each do |url|
      url = url.gsub(/^(ttp:\/\/|ttps:\/\/)/){|m|"h#{m}"}
      next if comment.check_and_add_image(url)
      next if comment.check_and_add_website(url)
      next if comment.check_and_add_movie(url)
      Resque.enqueue GetHttpContent, comment.id, url
    end
  end

end

