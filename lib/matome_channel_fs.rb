class MatomeChannelFS

  @@mg = nil

  def self.connect
    return @@mg if @@mg
    hosts = Settings.mogilefs.servers.map{|h|"#{h.address}:#{h.port}"}
    @@mg = MogileFS::MogileFS.new hosts: hosts,
      domain: Settings.mogilefs.domain
  end

  def self.set key, value
    self.connect.store_content key, :default, value
  end

  def self.set_file key, value
    self.connect.store_file key, :default, value
  end

  def self.get key
    self.connect.get_file_data key
  end

  def self.paths key
    self.connect.get_paths key
  end

  def self.delete key
    self.connect.delete key
  end
end

