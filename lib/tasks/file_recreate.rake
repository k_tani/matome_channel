# encoding: utf-8

namespace :storage do
  desc "file update when key changed"

  task :key_update => :environment do
    puts "start Movie thumb, animation recreate"
    Movie.all.each do |m|
      begin
        m.animation = MatomeChannelFS.get "#{m.id}_animation"  
        m.thumb = MatomeChannelFS.get "#{m.id}_thumb"  
        MatomeChannelFS.delete "#{m.id}_animation"  
        MatomeChannelFS.delete "#{m.id}_thumb"
      rescue => e
        puts e
      end
    end

    puts "start Website thumb recreate"
    Website.all.each do |w|
      begin
        w.thumb = MatomeChannelFS.get "#{w.id}_thumb"  
        MatomeChannelFS.delete "#{w.id}_thumb"
      rescue => e
        puts e
      end
    end


    puts "start Image thumb, image recreate"
    Image.all.each do |i|
      begin
        i.image = MatomeChannelFS.get "#{i.id}_image"
        i.thumb = MatomeChannelFS.get "#{i.id}_thumb"  
        MatomeChannelFS.delete "#{i.id}_image"  
        MatomeChannelFS.delete "#{i.id}_thumb"
      rescue => e
        puts e
      end
    end

  end
end

