# encoding: utf-8

namespace :hash_id_map do
  desc "recreate hash_id_map"

  task :recreate => :environment do
    HashIdMap.delete_all
    Board.all.each do |board|
      board.comments.order_by(num: :asc, created_by: :asc).each do |comment|
        comment.hash_id = HashIdMap.create_hash_id comment
        comment.save
      end
    end
  end
end

