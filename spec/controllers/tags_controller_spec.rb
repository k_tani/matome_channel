require 'spec_helper'

describe TagsController do
  before(:each) do
    board = FactoryGirl.create(:board, category: nil)
    FactoryGirl.create(:board_tag, board_ids: board.id)
    summary = FactoryGirl.create(:summary, category: nil)
    FactoryGirl.create(:summary_tag, summary_ids: summary.id)
  end

  it "response should be success" do
    get :index
    response.should be_success
  end

  it "response should be success sort use" do
    get :index, sort: "use"
    response.should be_success
  end

  it "response should be success category summary" do
    get :index, type: "summary"
    response.should be_success
  end

  it "response should be success category summary" do
    get :index, sort: "use", type: "summary"
    response.should be_success
  end
end
