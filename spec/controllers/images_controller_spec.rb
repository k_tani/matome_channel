require 'spec_helper'

describe ImagesController do

  describe "image" do
    it "render 404 error when image not found" do
      get :image, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "send_data when proxy is invalid" do
      image = FactoryGirl.create(:image)
      Settings.mogilefs["nginx_proxy"] = false
      get :image, id: image.id
      expect(response.header["Content-Type"]).to eq(image.content_type)
      expect(response.body).to eq(File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") )
    end

    it "assingns x-reproxy-url in response header" do
      image = FactoryGirl.create(:image)
      Settings.mogilefs["nginx_proxy"] = true
      get :image, id: image.id
      expect(response.header["Content-Type"]).to eq(image.content_type)
      expect(response.header["X-Accel-Redirect"]).to eq("/reproxy")
      expect(image.image_paths.include?(response.header["x-reproxy-url"])).to eq(true)
    end
  end

  describe "thumb" do
    it "render 404 error when image not found" do
      get :thumb, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "send_data when proxy is invalid" do
      image = FactoryGirl.create(:image)
      Settings.mogilefs["nginx_proxy"] = false
      get :thumb, id: image.id
      expect(response.header["Content-Type"]).to eq(image.content_type)
      expect(response.body).to eq(File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") )
    end

    it "assingns x-reproxy-url in response header" do
      image = FactoryGirl.create(:image)
      Settings.mogilefs["nginx_proxy"] = true
      get :thumb, id: image.id
      expect(response.header["Content-Type"]).to eq(image.content_type)
      expect(response.header["X-Accel-Redirect"]).to eq("/reproxy")
      expect(image.thumb_paths.include?(response.header["x-reproxy-url"])).to eq(true)
    end
  end

end
