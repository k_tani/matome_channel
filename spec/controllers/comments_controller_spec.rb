# -*- coding: utf-8 -*-
require 'spec_helper'

describe CommentsController do
  describe "show" do
    it "return 404 error when id is invalid" do
      get :show, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    describe "with xhr request" do
      it "assgins comment when mode is invalid" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        xhr :get, :show, id: comment.id, mode: "hoge"
        expect(response).to render_template(partial: "comments/_comment")
        expect(assigns(:comment)).to eq(comment)
      end

      it "assgins comment when mode is comment" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        xhr :get, :show, id: comment.id, mode: "comment"
        expect(response).to render_template(partial: "comments/_comment")
        expect(assigns(:comment)).to eq(comment)
      end

      it "assgins comment when mode is summary" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        xhr :get, :show, id: comment.id, mode: "summaries"
        expect(response).to render_template(partial: "summaries/_comment")
        expect(assigns(:comment)).to eq(comment)
      end
    end

    describe "with normal request" do
      it "assgins comment and render show" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        get :show, id: comment.id
        expect(response).to render_template("show")
        expect(assigns(:comment)).to eq(comment)
      end

      it "assgins comment and render json" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        get :show, id: comment.id, format: :json
        expect(response).to be_success
        expect(assigns(:comment)).to eq(comment)
        expect(response.body).to eq(comment.to_json(only: [:_id, :num, :name, :body, :hash_id, :created_at, :good, :bad]))
      end
    end
  end

  describe "create" do
    describe "with invalid parameter" do
      it "render new template when format is html" do
        board = FactoryGirl.create(:board_with_config)
        comment_attr = {
          name: "",
          body: "",
          board_id: board.id
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, comment: comment_attr
        expect(response).to render_template("new")
        expect(assigns(:comment).errors).to_not be_empty
      end

      it "return json for error template when format is html" do
        board = FactoryGirl.create(:board_with_config)
        comment_attr = {
          name: "",
          body: "",
          board_id: board.id
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, comment: comment_attr, format: :json
        expect(response.status).to eq(422)
        json = JSON.parse response.body
        expect(json["body"]).to match_array(["レス内容は1文字以上で入力してください。"])
      end
    end

    describe "with valid parameter" do
      it "redirect to show template when format is html" do
        board = FactoryGirl.create(:board_with_config)
        comment_attr = {
          name: "hoge",
          body: "fuga",
          board_id: board.id
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, comment: comment_attr
        expect(response).to redirect_to(board.comments[0])
        expect(assigns(:comment).errors).to be_empty
      end

      it "return json for comment when format is json" do
        board = FactoryGirl.create(:board_with_config)
        comment_attr = {
          name: "hoge",
          body: "fuga",
          board_id: board.id
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, comment: comment_attr, format: :json
        expect(assigns(:comment).errors).to be_empty 
        json = JSON.parse response.body
        expect(json["_id"]).to eq(board.comments[0].id.to_s)
      end
    end
  end

  describe "good" do
    it "return 404 error when id is invalid" do
      post :good, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    describe "when user signed in" do
      login_user

      it "return json which has increment good" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        post :good, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(json["good"]).to eq(1)
        expect(assigns(:current_user).get_check_list[comment.id.to_s]).to eq(1)
      end
    end

    describe "when user don't signed in" do
      it "return json which has increment good" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        post :good, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(json["good"]).to eq(1)
        expect(session["check_list"][comment.id]).to eq(1)
      end
    end
  end

  describe "bad" do
    it "return 404 error when id is invalid" do
      post :bad, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    describe "when user signed in" do
      login_user

      it "return json which has increment bad" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        post :bad, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(json["bad"]).to eq(1)
        expect(assigns(:current_user).get_check_list[comment.id.to_s]).to eq(0)
      end
    end

    describe "when user don't signed in" do
      it "return json which has increment bad" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        post :bad, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(json["bad"]).to eq(1)
        expect(session["check_list"][comment.id]).to eq(0)
      end
    end
  end

  describe "add_ng" do
    it "return 404 error when id is invalid" do
      post :add_ng, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    describe "when user signed in" do
      login_user

      it "return json which is ok" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        post :add_ng, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(assigns(:current_user).get_ng_list[comment.hash_id]).to eq(true)
        expect(json["result"]).to eq("ok")
      end
    end

    describe "when user don't signed in" do
      it "return json which is ok" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        post :add_ng, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(session["ng_list"][comment.hash_id]).to eq(true)
        expect(json["result"]).to eq("ok")
      end 
    end
  end

  describe "remove_ng" do
    it "return 404 error when id is invalid" do
      delete :add_ng, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end
    describe "when user signed in" do
      login_user

      it "return json which is ok" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        delete :remove_ng, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(assigns(:current_user).get_ng_list[comment.hash_id]).to_not eq(true)
        expect(json["result"]).to eq("ok")
      end
    end

    describe "when user don't signed in" do
      it "return json which is ok" do
        board = FactoryGirl.create(:board_with_config)
        comment = FactoryGirl.create(:comment, board: board)
        delete :remove_ng, id: comment.id
        expect(response).to be_success
        json = JSON.parse response.body
        expect(session["ng_list"][comment.hash_id]).to_not eq(true)
        expect(json["result"]).to eq("ok")
      end 
    end
  end

  describe "add_tag" do
    it "return 404 error when id is invalid" do
      post :add_tag, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "return json which is ok" do
      board = FactoryGirl.create(:board_with_config)
      comment = FactoryGirl.create(:comment, board: board) 
      post :add_tag, id: comment.id, tag_list: "hoge,fuga"
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["result"]).to eq("ok")
      expect(comment.reload.tag_list).to eq("hoge,fuga")
    end
  end

  describe "remove_tag" do
    it "return 404 error when id is invalid" do
      delete :remove_tag, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "return 404 error when tag_id is invalid" do
      board = FactoryGirl.create(:board_with_config)
      comment = FactoryGirl.create(:comment, board: board) 
      comment.tags << FactoryGirl.create(:comment_tag)
      delete :remove_tag, id: comment.id, tag_id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end 

    it "return json which is ok" do
      board = FactoryGirl.create(:board_with_config)
      comment = FactoryGirl.create(:comment, board: board) 
      comment.tags << FactoryGirl.create(:comment_tag)
      delete :remove_tag, id: comment.id, tag_id: comment.tags[0].id
      expect(response).to be_success
      json = JSON.parse response.body
      expect(json["result"]).to eq("ok")
      expect(comment.reload.tags).to be_empty
    end
  end


end
