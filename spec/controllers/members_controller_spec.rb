require 'spec_helper'

describe MembersController do

  describe "board_favorites" do
    it "render 404 error when user not found" do
      get :board_favorites, id: :hogehoge
      expect(response).to render_template("shared/error_404")
    end

    it "assgins favorites" do
      user = FactoryGirl.create(:user)
      board = FactoryGirl.create(:board, user: user)
      target = FactoryGirl.create(:board_favorite, board: board, user: user)
      other = FactoryGirl.create(:board_favorite)
      get :board_favorites, id: user.id
      expect(response).to be_success
      expect(assigns(:favorites)).to match_array([target])
    end
  end

  describe "summary_favorites" do
    it "render 404 error when user not found" do
      get :summary_favorites, id: :hogehoge
      expect(response).to render_template("shared/error_404")
    end
    it "assgins favorites" do
      user = FactoryGirl.create(:user)
      summary = FactoryGirl.create(:summary, user: user)
      target = FactoryGirl.create(:summary_favorite, summary: summary, user: user)
      other = FactoryGirl.create(:summary_favorite)
      get :summary_favorites, id: user.id
      expect(response).to be_success
      expect(assigns(:favorites)).to match_array([target])
    end
  end

  describe "boards" do
    it "render 404 error when user not found" do
      get :boards, id: :hogehoge
      expect(response).to render_template("shared/error_404")
    end

    it "assgins boards" do
      user = FactoryGirl.create(:user)
      target = FactoryGirl.create(:board, user: user)
      other = FactoryGirl.create(:board)
      get :boards, id: user.id
      expect(response).to be_success
      expect(assigns(:boards)).to match_array([target])
    end
  end

  describe "summaries" do
    it "render 404 error when user not found" do
      get :summaries, id: :hogehoge
      expect(response).to render_template("shared/error_404")
    end

    it "assgins summaries" do
      user = FactoryGirl.create(:user)
      target = FactoryGirl.create(:summary, user: user)
      other = FactoryGirl.create(:summary)
      get :summaries, id: user.id
      expect(response).to be_success
      expect(assigns(:summaries)).to match_array([target])
    end

  end

end
