require 'spec_helper'

describe FavoritesController do

  describe "boards" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :boards
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "assgins current_user boards" do
        user = User.last
        my_board = FactoryGirl.create(:board)
        other_board = FactoryGirl.create(:board)
        user.board_favorites << FactoryGirl.create(:board_favorite, board: my_board, user: user)
        get :boards 
        expect(response).to be_success
        expect(assigns(:user)).to eq(user)
        expect(assigns(:favorites).map(&:board)).to match_array([my_board])
      end
    end
  end

  describe "summaries" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :summaries
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "assgins current_user summaries" do
        user = User.last
        board = FactoryGirl.create(:board_with_comments)
        my_summary = FactoryGirl.create(:summary, board: board, user: user)
        other_summary = FactoryGirl.create(:summary, board: board, user: user)
        user.summary_favorites << FactoryGirl.create(:summary_favorite, summary: my_summary, user: user)
        get :summaries 
        expect(response).to be_success
        expect(assigns(:user)).to eq(user)
        expect(assigns(:favorites).map(&:summary)).to match_array([my_summary])
      end
    end
  end

  describe "create_board" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        board = FactoryGirl.create(:board_with_comments)
        post :create_board, board_id: board.id
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "render 404 error when board is invalid" do
        board = FactoryGirl.create(:board_with_comments)
        post :create_board, board_id: "hogehoge"
        expect(response).to render_template("shared/error_404")
      end

      it "assigns current_user board favorites" do
        board = FactoryGirl.create(:board_with_comments)
        post :create_board, board_id: board.id
        expect(response).to be_success
        expect(JSON.parse(response.body)["id"]).to_not eq(nil)
        expect(assigns(:current_user).board_favorites.last.board).to eq(board)
      end
    end
  end

  describe "create_summary" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        summary = FactoryGirl.create(:summary)
        post :create_summary, summary_id: summary.id
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user
      it "render 404 error when summary is invalid" do
        post :create_summary, summary_id: "hogehoge"
        expect(response).to render_template("shared/error_404")
      end

      it "assigns current_user summary favorites" do
        summary = FactoryGirl.create(:summary)
        post :create_summary, summary_id: summary.id
        expect(response).to be_success
        expect(JSON.parse(response.body)["id"]).to_not eq(nil)
        expect(assigns(:current_user).summary_favorites.last.summary).to eq(summary)
      end
    end
  end

  describe "destroy_board" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        board_favorite = FactoryGirl.create(:board_favorite)
        delete :destroy_board, id: board_favorite.id
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user
      it "render 404 error when favorite is invalid" do
        delete :destroy_board, id: "hogehoge"
        expect(response).to render_template("shared/error_404")
      end

      it "assigns current_user board favorites" do
        board_favorite = FactoryGirl.create(:board_favorite, user: User.last)
        post :destroy_board, id: board_favorite.id
        expect(response).to be_success
        expect(JSON.parse(response.body)["result"]).to eq("ok")
        expect(User.last.board_favorites).to be_empty
      end
    end
  end

  describe "destroy_summary" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        summary_favorite = FactoryGirl.create(:summary_favorite)
        delete :destroy_summary, id: summary_favorite.id
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user
      it "render 404 error when favorite is invalid" do
        delete :destroy_summary, id: "hogehoge"
        expect(response).to render_template("shared/error_404")
      end

      it "assigns current_user summary favorites" do
        summary_favorite = FactoryGirl.create(:summary_favorite, user: User.last)
        post :destroy_summary, id: summary_favorite.id
        expect(response).to be_success
        expect(JSON.parse(response.body)["result"]).to eq("ok")
        expect(User.last.summary_favorites).to be_empty
      end
    end
  end

end
