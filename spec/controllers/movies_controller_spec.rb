require 'spec_helper'

describe MoviesController do
  describe "animation" do
    it "render 404 error when image not found" do
      get :animation, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "send_data when proxy is invalid" do
      movie = FactoryGirl.create(:movie)
      Settings.mogilefs["nginx_proxy"] = false
      get :animation, id: movie.id
      expect(response.header["Content-Type"]).to eq("image/gif")
      expect(response.body).to eq(File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") )
    end

    it "assingns x-reproxy-url in response header" do
      movie = FactoryGirl.create(:movie)
      Settings.mogilefs["nginx_proxy"] = true
      get :animation, id: movie.id
      expect(response.header["Content-Type"]).to eq("image/gif")
      expect(response.header["X-Accel-Redirect"]).to eq("/reproxy")
      expect(movie.animation_paths.include?(response.header["x-reproxy-url"])).to eq(true)
    end
  end

  describe "thumb" do
    it "render 404 error when image not found" do
      get :thumb, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "send_data when proxy is invalid" do
      movie = FactoryGirl.create(:movie)
      Settings.mogilefs["nginx_proxy"] = false
      get :thumb, id: movie.id
      expect(response.header["Content-Type"]).to eq("image/jpeg")
      expect(response.body).to eq(File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") )
    end

    it "assingns x-reproxy-url in response header" do
      movie = FactoryGirl.create(:movie)
      Settings.mogilefs["nginx_proxy"] = true
      get :thumb, id: movie.id
      expect(response.header["Content-Type"]).to eq("image/jpeg")
      expect(response.header["X-Accel-Redirect"]).to eq("/reproxy")
      expect(movie.thumb_paths.include?(response.header["x-reproxy-url"])).to eq(true)
    end
  end
end
