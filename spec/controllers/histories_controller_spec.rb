require 'spec_helper'

describe HistoriesController do

  describe "my_boards" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :my_boards
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "assigns boards" do
        user = User.desc(:created_at).last
        target = FactoryGirl.create(:board, user: user)
        other = FactoryGirl.create(:board)
        get :my_boards
        expect(response).to be_success
        expect(assigns(:boards)).to match_array([target])
      end 
    end
  end

  describe "my_comments" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :my_comments
        expect(response).to redirect_to(new_user_session_path)
      end
   end

    describe "when user signed in" do
      login_user

      it "assigns comments" do
        user = User.desc(:created_at).last
        target = FactoryGirl.create(:comment, user: user)
        other = FactoryGirl.create(:comment)
        get :my_comments
        expect(response).to be_success
        expect(assigns(:comments)).to match_array([target])
      end 
     end
  end

  describe "my_summaries" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :my_summaries
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user
      it "assigns summaries" do
        user = User.desc(:created_at).last
        target = FactoryGirl.create(:summary, user: user)
        other = FactoryGirl.create(:summary)
        get :my_summaries
        expect(response).to be_success
        expect(assigns(:summaries)).to match_array([target])
      end
    end
  end

  describe "boards" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :boards
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user
      it "assigns boards" do
        user = User.desc(:created_at).last
        target = FactoryGirl.create(:board, user: user)
        other = FactoryGirl.create(:board)
        user.refer_history.push target
        get :boards
        expect(response).to be_success
        expect(assigns(:boards).first[:title]).to eq(target.title)
        expect(assigns(:boards).first[:id]).to eq(target.id)
        expect(assigns(:boards).first[:refered_at]).to_not eq(nil)
      end
    end
  end

  describe "summaries" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :summaries
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "assigns summaries" do
        user = User.desc(:created_at).last
        target = FactoryGirl.create(:summary, user: user)
        other = FactoryGirl.create(:summary)
        user.refer_history.push target
        get :summaries
        expect(response).to be_success
        expect(assigns(:summaries).first[:title]).to eq(target.title)
        expect(assigns(:summaries).first[:id]).to eq(target.id)
        expect(assigns(:summaries).first[:refered_at]).to_not eq(nil)
      end
    end
  end

end
