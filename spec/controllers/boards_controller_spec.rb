# -*- coding: utf-8 -*-
require 'spec_helper'

describe BoardsController do
  describe "index" do
    it "404 error when category not found" do
      get :index, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns no boards when category isn't selected" do
      get :index

      expect(response).to be_success
      expect(assigns(:boards)).to be_empty
      expect(assigns(:new_board_path)).to eq(new_board_path)
    end

    it "assigns no boards when category is selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      get :index, id: child_categories[1].id

      expect(response).to be_success
      expect(assigns(:boards)).to be_empty
      expect(assigns(:category)).to eq(child_categories[1])
      expect(assigns(:new_board_path)).to eq(new_category_board_path(child_categories[1]))
    end 

    it "assigns some boards when category isn't selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      boards = []
      boards << FactoryGirl.create(:board, category: nil)
      boards << FactoryGirl.create(:board, category: root_category)
      boards << FactoryGirl.create(:board, category: child_categories[0])
      boards << FactoryGirl.create(:board, category: child_categories[1])
      boards << FactoryGirl.create(:board, category: child_categories[2])
      get :index

      expect(response).to be_success
      expect(assigns(:boards)).to match_array(boards)
    end

    it "assigns some boards when root category is selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      boards = []
      boards << FactoryGirl.create(:board, category: nil)
      boards << FactoryGirl.create(:board, category: root_category)
      boards << FactoryGirl.create(:board, category: child_categories[0])
      boards << FactoryGirl.create(:board, category: child_categories[1])
      boards << FactoryGirl.create(:board, category: child_categories[2])
      get :index, id: root_category.id

      expect(response).to be_success
      expect(assigns(:boards).to_a).to match_array([boards[4],boards[3],boards[2],boards[1]])
      expect(assigns(:new_board_path)).to eq(new_category_board_path(root_category))
    end 

    it "assigns some boards when child category is selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      boards = []
      boards << FactoryGirl.create(:board, category: nil)
      boards << FactoryGirl.create(:board, category: root_category)
      boards << FactoryGirl.create(:board, category: child_categories[0])
      boards << FactoryGirl.create(:board, category: child_categories[1])
      boards << FactoryGirl.create(:board, category: child_categories[2])
      get :index, id: child_categories.last.id

      expect(response).to be_success
      expect(assigns(:boards).to_a).to match_array([boards.last])
      expect(assigns(:new_board_path)).to eq(new_category_board_path(child_categories.last.id))
    end 

  end

  describe "tag" do
    it "404 error when tag not found" do
      get :tag, tag_id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns no boards" do
      btag = FactoryGirl.create(:board_tag)
      board = FactoryGirl.create(:board)
      get :tag, tag_id: btag.id

      expect(response).to be_success
      expect(assigns(:boards).to_a).to be_empty
    end

    it "assigns some boards" do
      tagged_board = FactoryGirl.create(:board_with_tags)
      board = FactoryGirl.create(:board)

      get :tag, tag_id: tagged_board.tags.last.id
      expect(response).to be_success
      expect(assigns(:boards).to_a).to match_array([tagged_board])
    end
  end

  describe "new" do
    it "404 error when category not found" do
      get :new, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assign new board when category isn't selected" do
      get :new
      expect(response).to be_success
      expect(assigns(:board).category).to eq(nil)
      expect(assigns(:board).comments.length).to eq(1)
      expect(assigns(:board).tags).to be_empty
    end

    it "assign new board when category is selected" do
      category = FactoryGirl.create(:category)
      get :new, id: category.id
      expect(response).to be_success
      expect(assigns(:board).category).to eq(category)
      expect(assigns(:board).comments.length).to eq(1)
      expect(assigns(:board).tags).to be_empty
    end

  end

  describe "show" do 
    it "forwards 404 error when board isn't found" do
      get :show, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assign board" do
      board = FactoryGirl.create(:board_with_comments)
      hash_id_map = {}
      board.comments.each do |comment|
        hash_id_map[comment.hash_id] ||= []
        hash_id_map[comment.hash_id] << comment.id
      end
      get :show, id: board.id
      expect(response).to be_success
      expect(assigns(:board)).to eq(board)
      expect(assigns(:comments)).to match_array(board.comments.desc(:created_at).reverse)
      expect(assigns(:hash_id_map)).to eq(hash_id_map)
      expect(assigns(:websites)).to match_array(board.board_websites.desc(:created_at).reverse)
      expect(assigns(:images)).to match_array(board.board_images.desc(:created_at).reverse)
      expect(assigns(:movies)).to match_array(board.board_movies.desc(:created_at).reverse)
    end
  end

  describe "create" do
    describe "with invalid parameters" do
      it "alert when board title is empty" do
        board_attrs = {
          title: "",
          category_id: "",
          tag_list: "",
          configs_attributes: {
            "0" => {
              key: "default_name",
              value: "774"
            },
          },
          comments_attributes: {
            "0" => {
              name: "test",
              body: "hogehoge"
            }
          }, 
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, board: board_attrs
        
        expect(response).to render_template("new")
        expect(assigns(:board).errors.first[0]).to eq(:title)
      end

      it "alert when board title length over 256 words" do
        board_attrs = {
          title: ("a"*256),
          category_id: "",
          tag_list: "",
          configs_attributes: {
            "0" => {
              key: "default_name",
              value: "774"
            },
          },
          comments_attributes: {
            "0" => {
              name: "test",
              body: "hogehoge"
            }
          }, 
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, board: board_attrs
        
        expect(response).to render_template("new")
        expect(assigns(:board).errors.first[0]).to eq(:title)
      end

      it "alert when comment body is empty" do
        board_attrs = {
          title: "hogehoge",
          category_id: "",
          tag_list: "",
          configs_attributes: {
            "0" => {
              key: "default_name",
              value: "774"
            },
          },
          comments_attributes: {
            "0" => {
              name: "test",
              body: ""
            }
          }, 
        }
        request.env['User-Agent'] = 'hogehoge'
        request.env['REMOTE_HOST'] = 'hogehoge'
        post :create, board: board_attrs
        
        expect(response).to render_template("new")
        expect(assigns(:board).errors.first[0]).to eq(:comments)
      end

    end

    describe "with valid parameters" do
      describe "and user don't signed in" do
        it "assign board when category selected" do
          category = FactoryGirl.create(:category)
          board_attrs = {
            title: "hogehoge",
            category_id: category.id,
            tag_list: "hoge,fuga",
            configs_attributes: {
              "0" => {
                key: "default_name",
                value: "774"
              },
            },
            comments_attributes: {
              "0" => {
                name: "",
                body: "testes"
              }
            }, 
          }
          request.env['User-Agent'] = 'hogehoge-agent'
          request.env['REMOTE_HOST'] = 'hogehoge-host'
          post :create, board: board_attrs
          expect(response).to redirect_to(assigns(:board))
          expect(assigns(:board).category).to eq(category)
          expect(assigns(:board).title).to eq("hogehoge")
          expect(assigns(:board).tag_list).to eq("hoge,fuga")
          expect(assigns(:board).user).to eq(nil)
          expect(assigns(:board).comments.first.user).to eq(nil)
          expect(assigns(:board).comments.first.name).to eq("774")
          expect(assigns(:board).comments.first.body).to eq("testes")
          expect(assigns(:board).comments.first.remote_host).to eq("hogehoge-host")
          expect(assigns(:board).comments.first.user_agent).to eq("hogehoge-agent")
        end

        it "assign board when category unselected" do
          board_attrs = {
            title: "hogehoge",
            category_id: "",
            tag_list: "hoge,fuga",
            configs_attributes: {
              "0" => {
                key: "default_name",
                value: "774"
              },
            },
            comments_attributes: {
              "0" => {
                name: "fugafuga",
                body: "testes"
              }
            }, 
          }
          request.env['User-Agent'] = 'hogehoge-agent'
          request.env['REMOTE_HOST'] = 'hogehoge-host'
          post :create, board: board_attrs
          
          expect(response).to redirect_to(board_url(id: assigns(:board).id))
          expect(assigns(:board).category).to eq(nil)
          expect(assigns(:board).title).to eq("hogehoge")
          expect(assigns(:board).tag_list).to eq("hoge,fuga")
          expect(assigns(:board).user).to eq(nil)
          expect(assigns(:board).comments.first.user).to eq(nil)
          expect(assigns(:board).comments.first.name).to eq("fugafuga")
          expect(assigns(:board).comments.first.body).to eq("testes")
          expect(assigns(:board).comments.first.remote_host).to eq("hogehoge-host")
          expect(assigns(:board).comments.first.user_agent).to eq("hogehoge-agent")
        end
      end

      describe "and user signed in" do
        login_user
        it "assign board" do
          category = FactoryGirl.create(:category)
          board_attrs = {
            title: "hogehoge",
            category_id: category.id,
            tag_list: "hoge,fuga",
            configs_attributes: {
              "0" => {
                key: "default_name",
                value: "774"
              },
            },
            comments_attributes: {
              "0" => {
                name: "",
                body: "testes"
              }
            }, 
          }
          request.env['User-Agent'] = 'hogehoge-agent'
          request.env['REMOTE_HOST'] = 'hogehoge-host' 
          post :create, board: board_attrs
          
          expect(response).to redirect_to(board_path(assigns(:board)))
          expect(assigns(:board).category).to eq(category)
          expect(assigns(:board).title).to eq("hogehoge")
          expect(assigns(:board).tag_list).to eq("hoge,fuga")
          expect(assigns(:board).user).not_to eq(nil)
          expect(assigns(:board).comments.first.user).not_to eq(nil)
          expect(assigns(:board).comments.first.name).to eq("774")
          expect(assigns(:board).comments.first.body).to eq("testes")
          expect(assigns(:board).comments.first.remote_host).to eq("hogehoge-host")
          expect(assigns(:board).comments.first.user_agent).to eq("hogehoge-agent")
        end
      end
    end
  end

  describe "comments" do 
    it "return 404 error when board isn't found" do
      get :comments, id: "hogehoge", num: "hoge"
      expect(response).to render_template("shared/error_404")
    end

    describe "with xhr request" do
      it "assigns boards when num is invalid" do
        board = FactoryGirl.create(:board_with_comments)
        hash_id_map = {}
        board.comments.each do |comment|
          hash_id_map[comment.hash_id] ||= []
          hash_id_map[comment.hash_id] << comment.id
        end
        xhr :get, :comments, id: board.id, num: "hoge"
    
        expect(response).to be_success
        expect(response).to render_template(partial: "comments/_list")
        expect(assigns(:board)).to eq(board)
        expect(assigns(:comments)).to eq(board.comments.reverse)
        expect(assigns(:hash_id_map)).to eq(hash_id_map)
        expect(assigns(:websites)).to eq(nil)
        expect(assigns(:images)).to eq(nil)
        expect(assigns(:movies)).to eq(nil)
      end

      it "assigns boards when num is minimum" do
        board = FactoryGirl.create(:board_with_comments)
        hash_id_map = {}
        board.comments.each do |comment|
          hash_id_map[comment.hash_id] ||= []
          hash_id_map[comment.hash_id] << comment.id
        end
        xhr :get, :comments, id: board.id, num: "2-"
    
        expect(response).to be_success
        expect(response).to render_template(partial: "comments/_list")
        expect(assigns(:board)).to eq(board)
        expect(assigns(:comments)).to match_array(board.comments.where(:num.gte => 2).reverse)
        expect(assigns(:hash_id_map)).to eq(hash_id_map)
        expect(assigns(:websites)).to eq(nil)
        expect(assigns(:images)).to eq(nil)
        expect(assigns(:movies)).to eq(nil)
      end

      it "assigns boards when num is maximum" do
        board = FactoryGirl.create(:board_with_comments)
        hash_id_map = {}
        board.comments.each do |comment|
          hash_id_map[comment.hash_id] ||= []
          hash_id_map[comment.hash_id] << comment.id
        end
        xhr :get, :comments, id: board.id, num: "-2"
    
        expect(response).to be_success
        expect(response).to render_template(partial: "comments/_list")
        expect(assigns(:board)).to eq(board)
        expect(assigns(:comments)).to match_array(board.comments.where(:num.lte => 2).reverse)
        expect(assigns(:hash_id_map)).to eq(hash_id_map)
        expect(assigns(:websites)).to eq(nil)
        expect(assigns(:images)).to eq(nil)
        expect(assigns(:movies)).to eq(nil)
      end

    end
    
    describe "with normal request" do
      it "assigns boards when num is invalid" do
        board = FactoryGirl.create(:board_with_comments)
        hash_id_map = {}
        board.comments.each do |comment|
          hash_id_map[comment.hash_id] ||= []
          hash_id_map[comment.hash_id] << comment.id
        end
        get :comments, id: board.id, num: "hoge"
    
        expect(response).to be_success
        expect(response).to render_template("show")
        expect(assigns(:board)).to eq(board)
        expect(assigns(:comments)).to match_array(board.comments.reverse)
        expect(assigns(:hash_id_map)).to eq(hash_id_map)
        expect(assigns(:websites)).to match_array(board.board_websites.desc(:created_at).to_a)
        expect(assigns(:images)).to match_array(board.board_images.desc(:created_at).to_a)
        expect(assigns(:movies)).to match_array(board.board_movies.desc(:created_at).to_a)
      end

      it "assigns boards when num is minimum" do
        board = FactoryGirl.create(:board_with_comments)
        hash_id_map = {}
        board.comments.each do |comment|
          hash_id_map[comment.hash_id] ||= []
          hash_id_map[comment.hash_id] << comment.id
        end
        get :comments, id: board.id, num: "2-"
    
        expect(response).to be_success
        expect(response).to render_template("show")
        expect(assigns(:board)).to eq(board)
        expect(assigns(:comments)).to match_array(board.comments.where(:num.gte => 2).reverse)
        expect(assigns(:hash_id_map)).to eq(hash_id_map)
        expect(assigns(:websites)).to match_array(board.board_websites.desc(:created_at).to_a)
        expect(assigns(:images)).to match_array(board.board_images.desc(:created_at).to_a)
        expect(assigns(:movies)).to match_array(board.board_movies.desc(:created_at).to_a)
      end

      it "assigns boards when num is maximum" do
        board = FactoryGirl.create(:board_with_comments)
        hash_id_map = {}
        board.comments.each do |comment|
          hash_id_map[comment.hash_id] ||= []
          hash_id_map[comment.hash_id] << comment.id
        end
        get :comments, id: board.id, num: "-2"
    
        expect(response).to be_success
        expect(response).to render_template("show")
        expect(assigns(:board)).to eq(board)
        expect(assigns(:comments)).to match_array(board.comments.where(:num.lte => 2).reverse)
        expect(assigns(:hash_id_map)).to eq(hash_id_map)
        expect(assigns(:websites)).to match_array(board.board_websites.desc(:created_at).to_a)
        expect(assigns(:images)).to match_array(board.board_images.desc(:created_at).to_a)
        expect(assigns(:movies)).to match_array(board.board_movies.desc(:created_at).to_a)
      end

    end

  end

  describe "websites" do 
    it "return 404 error when board isn't found" do
      get :websites, id: "hogehoge", num: "hoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns websites when num is invalid" do
      board = FactoryGirl.create(:board_with_comments)
      get :websites, id: board.id, num: "aa"
      expect(response).to render_template(partial: "websites/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:websites)).to match_array(board.board_websites.desc(:created_at).to_a) 
    end

    it "assigns websites when num is minimum" do
      board = FactoryGirl.create(:board_with_comments)
      get :websites, id: board.id, num: "#{board.board_websites[1].id}-"
      expect(response).to render_template(partial: "websites/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:websites).to_a).to match_array([board.board_websites.desc(:created_at)[0]]) 
    end

    it "assigns websites when num is maximum" do
      board = FactoryGirl.create(:board_with_comments)
      get :websites, id: board.id, num: "-#{board.board_websites[1].id}"
      expect(response).to render_template(partial: "websites/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:websites).to_a).to match_array([board.board_websites.desc(:created_at)[2]]) 
    end
  end

  describe "images" do 
    it "return 404 error when board isn't found" do
      get :images, id: "hogehoge", num: "hoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns images when num is invalid" do
      board = FactoryGirl.create(:board_with_comments)
      get :images, id: board.id, num: "aa"
      expect(response).to render_template(partial: "images/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:images)).to match_array(board.board_images.desc(:created_at).to_a) 
    end

    it "assigns images when num is minimum" do
      board = FactoryGirl.create(:board_with_comments)
      get :images, id: board.id, num: "#{board.board_images[1].id}-"
      expect(response).to render_template(partial: "images/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:images).to_a).to match_array([board.board_images.desc(:created_at)[0]]) 
    end

    it "assigns images when num is maximum" do
      board = FactoryGirl.create(:board_with_comments)
      get :images, id: board.id, num: "-#{board.board_images[1].id}"
      expect(response).to render_template(partial: "images/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:images).to_a).to match_array([board.board_images.desc(:created_at)[2]]) 
    end

  end

  describe "movies" do 
    it "return 404 error when board isn't found" do
      get :movies, id: "hogehoge", num: "hoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns movies when num is invalid" do
      board = FactoryGirl.create(:board_with_comments)
      get :movies, id: board.id, num: "aa"
      expect(response).to render_template(partial: "movies/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:movies)).to match_array(board.board_movies.desc(:created_at).to_a) 
    end

    it "assigns movies when num is minimum" do
      board = FactoryGirl.create(:board_with_comments)
      get :movies, id: board.id, num: "#{board.board_movies[1].id}-"
      expect(response).to render_template(partial: "movies/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:movies).to_a).to match_array([board.board_movies.desc(:created_at)[0]]) 
    end

    it "assigns movies when num is maximum" do
      board = FactoryGirl.create(:board_with_comments)
      get :movies, id: board.id, num: "-#{board.board_movies[1].id}"
      expect(response).to render_template(partial: "movies/_list")
      expect(assigns(:board)).to eq(board) 
      expect(assigns(:movies).to_a).to match_array([board.board_movies.desc(:created_at)[2]]) 
    end

  end

  describe "add_tag" do 
    it "return 404 error when board isn't found" do
      post :add_tag, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "render json" do
      board = FactoryGirl.create(:board)
      post :add_tag, id: board.id, tag_list: "hoge,fuga"
      json = JSON.parse response.body
      expect(json["result"]).to eq("ok")
      expect(assigns(:board).tag_list).to eq("hoge,fuga")
    end
  end

  describe "remove_tag" do 
    it "return 404 error when board isn't found" do
      delete :remove_tag, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "render json" do
      board = FactoryGirl.create(:board)
      board.tags << FactoryGirl.create(:board_tag)
      delete :remove_tag, id: board.id, tag_id: board.tags.first.id
      json = JSON.parse response.body
      expect(json["result"]).to eq("ok")
      expect(assigns(:board).tags).to be_empty
    end
  end

end

