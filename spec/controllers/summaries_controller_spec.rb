require 'spec_helper'

describe SummariesController do
  describe "index" do
    it "404 error when category not found" do
      get :index, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns no summaries when category isn't selected" do
      get :index

      expect(response).to be_success
      expect(assigns(:summaries)).to be_empty
    end

    it "assigns no summaries when category is selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      get :index, id: child_categories[1].id

      expect(response).to be_success
      expect(assigns(:summaries)).to be_empty
      expect(assigns(:category)).to eq(child_categories[1])
    end 

    it "assigns some summaries when category isn't selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      summaries = []
      summaries << FactoryGirl.create(:summary, category: nil)
      summaries << FactoryGirl.create(:summary, category: root_category)
      summaries << FactoryGirl.create(:summary, category: child_categories[0])
      summaries << FactoryGirl.create(:summary, category: child_categories[1])
      summaries << FactoryGirl.create(:summary, category: child_categories[2])
      get :index

      expect(response).to be_success
      expect(assigns(:summaries)).to match_array(summaries)
    end

    it "assigns some summaries when root category is selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      summaries = []
      summaries << FactoryGirl.create(:summary, category: nil)
      summaries << FactoryGirl.create(:summary, category: root_category)
      summaries << FactoryGirl.create(:summary, category: child_categories[0])
      summaries << FactoryGirl.create(:summary, category: child_categories[1])
      summaries << FactoryGirl.create(:summary, category: child_categories[2])
      get :index, id: root_category.id

      expect(response).to be_success
      expect(assigns(:summaries).to_a).to match_array([summaries[4],summaries[3],summaries[2],summaries[1]])
    end 

    it "assigns some summaries when child category is selected" do
      root_category = FactoryGirl.create(:category, parent: nil)
      child_categories = []
      3.times do
        child_categories << FactoryGirl.create(:category, parent: root_category)
      end
      summaries = []
      summaries << FactoryGirl.create(:summary, category: nil)
      summaries << FactoryGirl.create(:summary, category: root_category)
      summaries << FactoryGirl.create(:summary, category: child_categories[0])
      summaries << FactoryGirl.create(:summary, category: child_categories[1])
      summaries << FactoryGirl.create(:summary, category: child_categories[2])
      get :index, id: child_categories.last.id

      expect(response).to be_success
      expect(assigns(:summaries).to_a).to match_array([summaries.last])
    end 

  end

  describe "tag" do
    it "404 error when tag not found" do
      get :tag, tag_id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "assigns no summaries" do
      btag = FactoryGirl.create(:summary_tag)
      summary = FactoryGirl.create(:summary)
      get :tag, tag_id: btag.id

      expect(response).to be_success
      expect(assigns(:summaries).to_a).to be_empty
    end

    it "assigns some summaries" do
      tagged_summary = FactoryGirl.create(:summary_with_tags)
      summary = FactoryGirl.create(:summary)

      get :tag, tag_id: tagged_summary.tags.last.id
      expect(response).to be_success
      expect(assigns(:summaries).to_a).to match_array([tagged_summary])
    end
  end

  describe "new" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        get :new, board_id: "hogehoge"
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "render 404 error when board not found" do
        get :new, board_id: "hogehoge"
        expect(response).to render_template("shared/error_404")
      end 

      it "assigns new summary" do
        board = FactoryGirl.create(:board_with_comments)
        tags = CommentTag.where(:comment_ids.in => board.comments.map(&:id))
        get :new, board_id: board.id
        expect(response).to be_success
        expect(assigns(:summary)).to_not eq(nil)
        expect(assigns(:summary).board).to eq(board)
        expect(assigns(:tags)).to eq(tags)
      end 
    end
  end

  describe "edit" do
    describe "when user don't sign in" do
      it "redirect to login page" do
        summary = FactoryGirl.create(:summary)
        get :edit, id: summary.id
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "redirect to root page when summary doesn't belongs to login user" do
        summary = FactoryGirl.create(:summary)
        get :edit, id: summary.id
        expect(response).to redirect_to(root_path)
      end 
      
      it "render 404 error when summary not found" do
        get :edit, id: "hogehoge"
        expect(response).to render_template("shared/error_404")
      end 

      it "assigns new summary" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, user: user)
        tags = CommentTag.where(:comment_ids.in => summary.board.comments.map(&:id))
        get :edit, id: summary.id
        expect(response).to be_success
        expect(assigns(:summary)).to eq(summary)
        expect(assigns(:comments)).to eq(summary.comments.to_a)
        expect(assigns(:hash_id_map)).to eq(HashIdMap.get_map summary.comments.map(&:hash_id).uniq, summary.board_id)
        expect(assigns(:tags)).to eq(tags)
      end
    end
  end

  describe "show" do

    it "render 404 error when summary not found" do
      get :show, id: "hogehoge"
      expect(response).to render_template("shared/error_404") 
    end

    describe "when user don't sign in" do
      it "redirect to root page when summary isn't published" do
        summary = FactoryGirl.create(:summary, publish: false)
        get :show, id: summary.id
        expect(response).to redirect_to(root_path)
      end

      it "assigns summary when summary is published" do
        summary = FactoryGirl.create(:summary)
        get :show, id: summary.id
        expect(response).to be_success
        expect(assigns(:summary)).to eq(summary)
        expect(assigns(:comments)).to eq(summary.comments.to_a)
        expect(assigns(:hash_id_map)).to eq(HashIdMap.get_map summary.comments.map(&:hash_id).uniq, summary.board_id)
      end
    end

    describe "when user don't sign in" do
      login_user

      it "redirect to login page when summary isn't published and doesn't belongs to login user" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, publish: false)
        get :edit, id: summary.id
        expect(response).to redirect_to(root_path)
      end 

      it "assigns summary when summary published and doesn't belongs to login user" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary)
        get :show, id: summary.id
        expect(response).to be_success
        expect(assigns(:summary)).to eq(summary)
        expect(assigns(:comments)).to eq(summary.comments.to_a)
        expect(assigns(:hash_id_map)).to eq(HashIdMap.get_map summary.comments.map(&:hash_id).uniq, summary.board_id)
      end

      it "assigns summary when summary isn't published and  belongs to login user" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, publish: false, user: user)
        get :show, id: summary.id
        expect(response).to be_success
        expect(assigns(:summary)).to eq(summary)
        expect(assigns(:comments)).to eq(summary.comments.to_a)
        expect(assigns(:hash_id_map)).to eq(HashIdMap.get_map summary.comments.map(&:hash_id).uniq, summary.board_id)
      end

      it "assigns summary when summary published and belongs to login user" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, user: user)
        get :show, id: summary.id
        expect(response).to be_success
        expect(assigns(:summary)).to eq(summary)
        expect(assigns(:comments)).to eq(summary.comments.to_a)
        expect(assigns(:hash_id_map)).to eq(HashIdMap.get_map summary.comments.map(&:hash_id).uniq, summary.board_id)
      end 
    end
  end

  describe "create" do
    describe "when user don't sign in" do
      it "redirect to login page when summary isn't published" do
        post :create, summary: {}
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user don't sign in" do
      login_user

      it "render new when format is html and invalid parameter" do
        board = FactoryGirl.create(:board_with_comments)
        params = {
          board_id: board.id,
          tag_list: "",
          publish: "0",
          title: "",
          description: "description 1"
        }
        post :create, summary: params
        expect(response).to render_template(:new)
        expect(assigns(:summary).errors.first[0]).to eq(:title)
      end

      it "render error json when format is json and invalid parameter" do
        board = FactoryGirl.create(:board_with_comments)
        params = {
          board_id: board.id,
          tag_list: "",
          publish: "0",
          title: "",
          description: "description 1"
        }
        post :create, summary: params, format: :json
        expect(response.status).to eq(422)
        expect(assigns(:summary).errors.first[0]).to eq(:title)
        expect(JSON.parse(response.body)["title"]).to_not be_empty
      end

      it "redirect to show when format is html and invalid parameter" do
        board = FactoryGirl.create(:board_with_comments)
        params = {
          board_id: board.id,
          publish: "0",
          tag_list: "hoge,fuga",
          title: "title 1",
          description: "description 1",
          items: {
            "0" => {
              comment_id: board.comments[0].id,
              decoration: {
                "size"=>"14",
                "color"=>"#333333",
                "bold"=>"false",
                "italic"=>"false",
                "comment"=>"hogehoge"
              },
            },
            "1" => {
              comment_id: board.comments[1].id,
              decoration: {
                "size"=>"14",
                "color"=>"#333333",
                "bold"=>"false",
                "italic"=>"false",
                "comment"=>""
              },
            }
          }
        }
        post :create, summary: params
        expect(response).to redirect_to(summary_path(assigns(:summary)))
        expect(assigns(:summary).board).to eq(board)
        expect(assigns(:summary).tag_list).to eq("hoge,fuga")
        expect(assigns(:summary).title).to eq("title 1")
        expect(assigns(:summary).description).to eq("description 1")
        expect(assigns(:summary).items[0]).to eq({
          "comment_id" => board.comments[0].id.to_s,
          "decoration" => {
            "size"=>"14",
            "color"=>"#333333",
            "bold"=>"false",
            "italic"=>"false",
            "comment"=>"hogehoge"
          }
        })
        expect(assigns(:summary).items[1]).to eq({
          "comment_id" => board.comments[1].id.to_s,
          "decoration" => {
            "size"=>"14",
            "color"=>"#333333",
            "bold"=>"false",
            "italic"=>"false",
            "comment"=>""
          }
        })
      end

      it "render ok json when format is json and invalid parameter" do
        board = FactoryGirl.create(:board_with_comments)
        params = {
          board_id: board.id,
          publish: "0",
          tag_list: "hoge,fuga",
          title: "title 1",
          description: "description 1",
          items: {
            "0" => {
              comment_id: board.comments[0].id,
              decoration: {
                "size"=>"14",
                "color"=>"#333333",
                "bold"=>"false",
                "italic"=>"false",
                "comment"=>"hogehoge"
              },
            },
            "1" => {
              comment_id: board.comments[1].id,
              decoration: {
                "size"=>"14",
                "color"=>"#333333",
                "bold"=>"false",
                "italic"=>"false",
                "comment"=>""
              },
            }
          }
        }
        post :create, summary: params, format: :json
        expect(response).to be_success
        json = JSON.parse(response.body)
        expect(json["board_id"]).to eq(board.id.to_s)
        expect(json["publish"]).to eq(false)
        expect(json["title"]).to eq("title 1")
        expect(json["description"]).to eq("description 1")
        expect(json["items"][0]).to eq({
          "comment_id" => board.comments[0].id.to_s,
          "decoration" => {
            "size"=>"14",
            "color"=>"#333333",
            "bold"=>"false",
            "italic"=>"false",
            "comment"=>"hogehoge"
          }
        })
        expect(json["items"][1]).to eq({
          "comment_id" => board.comments[1].id.to_s,
          "decoration" => {
            "size"=>"14",
            "color"=>"#333333",
            "bold"=>"false",
            "italic"=>"false",
            "comment"=>""
          }
        })


      end
    end
  end

  describe "update" do

    describe "when user don't sign in" do
      it "redirect to login page when summary isn't published" do
        put :update, id: "1", summary: {}
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    describe "when user signed in" do
      login_user

      it "render 404 error when summary not found" do
        put :update, id: "hogehoge"
        expect(response).to render_template("shared/error_404") 
      end 
    
      it "redirect to root_path when summary doesn't belongs to login user" do
        summary = FactoryGirl.create(:summary)
        put :update, id: summary.id, summary: {}
        expect(response).to redirect_to(root_path)
      end

      it "render edit when format is html and invalid parameter" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, user: user)
        params = {
          board_id: summary.board.id,
          publish: "0",
          tag_list: "hoge,fuga",
          title: "",
          description: "description 1",
          items: {
          } 
        }
        put :update, id: summary.id, summary: params
        expect(response).to render_template("edit") 
        expect(assigns(:summary).errors.first[0]).to eq(:title)
      end

      it "return 422 error when format is json and invalid parameter" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, user: user)
        params = {
          board_id: summary.board.id,
          publish: "0",
          tag_list: "hoge,fuga",
          title: "",
          description: "description 1",
          items: {
          } 
        }
        put :update, id: summary.id, summary: params, format: :json
        expect(response.status).to eq(422) 
        expect(JSON.parse(response.body)["title"]).to_not be_empty
      end

      it "redirect to show when format is html and valid parameter" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, user: user)
        params = {
          board_id: summary.board.id,
          publish: "1",
          tag_list: "hoge,fuga,fugo",
          title: "changed title",
          description: "changed description",
          items: {
            "0" => {
              comment_id: summary.board.comments[0].id,
              decoration: {
                "size"=>"20",
                "color"=>"#ffffff",
                "bold"=>"true",
                "italic"=>"true",
                "comment"=>"hogehogefugafuga"
              },
            }
          } 
        }
        put :update, id: summary.id, summary: params
        expect(response).to redirect_to(summary_path(assigns(:summary)))
        expect(assigns(:summary).title).to eq("changed title")
        expect(assigns(:summary).description).to eq("changed description")
        expect(assigns(:summary).publish).to eq(true)
        expect(assigns(:summary).items[0]).to eq({
          "comment_id" => summary.board.comments[0].id.to_s,
          "decoration" => {
            "size"=>"20",
            "color"=>"#ffffff",
            "bold"=>"true",
            "italic"=>"true",
            "comment"=>"hogehogefugafuga"
          }
        })
      end

      it "return success and json when format is json and valid parameter" do
        user = User.desc(:created_at).last
        summary = FactoryGirl.create(:summary, user: user)
        params = {
          board_id: summary.board.id,
          publish: "1",
          tag_list: "hoge,fuga,fugo",
          title: "changed title",
          description: "changed description",
          items: {
            "0" => {
              comment_id: summary.board.comments[0].id,
              decoration: {
                "size"=>"20",
                "color"=>"#ffffff",
                "bold"=>"true",
                "italic"=>"true",
                "comment"=>"hogehogefugafuga"
              },
            }
          } 
        }
        put :update, id: summary.id, summary: params, format: :json
        expect(response).to be_success
        json = JSON.parse response.body
        expect(json["title"]).to eq("changed title")
        expect(json["description"]).to eq("changed description")
        expect(json["publish"]).to eq(true)
        expect(json["items"][0]).to eq({
          "comment_id" => summary.board.comments[0].id.to_s,
          "decoration" => {
            "size"=>"20",
            "color"=>"#ffffff",
            "bold"=>"true",
            "italic"=>"true",
            "comment"=>"hogehogefugafuga"
          }
        }) 
      end

    end

  end

  describe "items" do
    it "render partail edit_list" do
      board = FactoryGirl.create(:board_with_comments)
      params = {
        board_id: board.id,
        publish: "0",
        tag_list: "hoge,fuga",
        title: "title 1",
        description: "description 1",
        conditions: {
          "0" => {
            good_lte: "1"
          }
        }
      }
      post :items, summary: params
      expect(response).to render_template(partial: "_edit_list")
    end
  end
end
