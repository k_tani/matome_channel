require 'spec_helper'

describe WebsitesController do
  describe "thumb" do
    it "render 404 error when image not found" do
      get :thumb, id: "hogehoge"
      expect(response).to render_template("shared/error_404")
    end

    it "send_data when proxy is invalid" do
      website = FactoryGirl.create(:website)
      Settings.mogilefs["nginx_proxy"] = false
      get :thumb, id: website.id
      expect(response.header["Content-Type"]).to eq("image/png")
      expect(response.body).to eq(File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") )
    end

    it "assingns x-reproxy-url in response header" do
      website = FactoryGirl.create(:website)
      Settings.mogilefs["nginx_proxy"] = true
      get :thumb, id: website.id
      expect(response.header["Content-Type"]).to eq("image/png")
      expect(response.header["X-Accel-Redirect"]).to eq("/reproxy")
      expect(website.thumb_paths.include?(response.header["x-reproxy-url"])).to eq(true)
    end
  end
end
