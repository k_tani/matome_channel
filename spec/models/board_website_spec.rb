require 'spec_helper'

describe BoardWebsite do

  it "#inc_count" do
    bw = FactoryGirl.create(:board_website)
    expect(bw.inc_count).to eq(1)
    expect(bw.reload.count).to eq(1)
  end

end
