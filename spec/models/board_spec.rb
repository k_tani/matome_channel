require 'spec_helper'

describe Board do

  describe "#first_comment" do
    it "return first comment when board has some comments" do
    end

    it "return nil when board doesn't have any comments" do
    end
  end

  describe "#build_config" do
    it "config first comment when board has some comments" do
    end
  end

  describe "#thumbnail" do
    it "return placeholder.png when all" do
    end

    it "return Image when image only found" do
    end

    it "return Website when website only found" do
    end
    
    it "return Movie when movie only found" do
    end

    it "return Image when all are found and same count" do
    end 

    it "return Image when image and movie found and same count" do
    end 

    it "return Movie when movie and website found and same count" do
    end

    it "return Website when all are found and website count is maximum" do
    end 

    it "return Movie when all are found and movie count is maximum" do
    end 
  end

  describe "#config" do
  end

  describe "#inc_count" do
  end

  describe "#inc_id_count" do
  end

  describe "#add_image" do
  end

  describe "#add_website" do
  end

  describe "#add_movie" do
  end

  describe "#notify_change_favorites" do
  end

  describe "#notify_change_tags" do
  end

  describe "#notify" do
  end
  
end
