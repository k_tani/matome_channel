require 'spec_helper'

describe BoardMovie do

  it "#inc_count" do
    bm = FactoryGirl.create(:board_movie)
    expect(bm.inc_count).to eq(1)
    expect(bm.reload.count).to eq(1)
  end

end
