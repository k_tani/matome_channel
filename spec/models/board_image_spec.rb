require 'spec_helper'

describe BoardImage do

  it "#inc_count" do
    bi = FactoryGirl.create(:board_image)
    expect(bi.inc_count).to eq(1)
    expect(bi.reload.count).to eq(1)
  end

end
