FactoryGirl.define do

  factory :category do
    sequence(:name){|n|"Category #{n}"}
  end

  factory :board do
    category
    sequence(:title) {|n| "Board #{n}" }

    factory :board_with_config do
      before(:create) do |board|
        board.configs << FactoryGirl.build(:config_default_name)
      end
    end

    factory :board_with_tags do
      after(:create) do |board|
        3.times do
          board.tags << FactoryGirl.create(:board_tag)
        end
      end
    end 

    factory :board_with_comments do
      after(:create) do |board|
        3.times do
          comment = FactoryGirl.create(:comment, board: board)
          website = FactoryGirl.create(:website)
          image = FactoryGirl.create(:image)
          movie = FactoryGirl.create(:movie)
          comment.websites << website
          comment.images << image
          comment.movies << movie
          comment.tags << FactoryGirl.create(:comment_tag)
          FactoryGirl.create(:board_website, board: board, website: website)
          FactoryGirl.create(:board_image, board: board, image: image)
          FactoryGirl.create(:board_movie, board: board, movie: movie)
          board.comments << comment
        end
      end
    end 
  end

  factory :comment do
    association :board, factory: :board_with_config
    sequence(:num) {|n| n+1 }
    sequence(:body) {|n| "comments #{n}" }
    good 0
    bad 0
    remote_host "localhost"
    user_agent "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:19.0) Gecko/20100101 Firefox/19.0"

    factory :comment_with_websites do
      after(:create) do |comment|
        3.times do
          comment.images << FactoryGirl.create(:website, comment: comment)
        end
      end
    end
    
    factory :comment_with_images do
      after(:create) do |comment|
        3.times do
          comment.images << FactoryGirl.create(:image, comment: comment)
        end
      end
    end

    factory :comment_with_movies do
      after(:create) do |comment|
        3.times do
          comment.images << FactoryGirl.create(:movie, comment: comment)
        end
      end
    end
  end

  factory :board_website do
    count 0
    board
    website
  end

  factory :board_image do
    count 0
    image
    board
  end

  factory :board_movie do
    count 0
    movie
    board
  end

  factory :website do
    url "http://example.com"
    sequence(:title) {|n| "Website #{n}" }
    thumb { File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") }
  end

  factory :image do
    url "http://example.com"
    content_type "image/jpeg"
    thumb { File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") }
    image { File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") }
  end

  factory :movie do
    trait :youtube do
      media "youtube"
      url "http://"
    end

    trait :nicovideo do
      media "nicovideo"
      url "http://"
    end
    animation { File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") }
    thumb { File.read(File.join([Rails.root, "spec", "files", "file001.jpg"])).force_encoding("ASCII-8BIT") }
    factory :youtube_movie, traits: [:youtube]
    factory :nicovideo_movie, traits: [:nicovideo]
  end

  factory :user do
    sequence(:name){|n| "user name #{n}"}
    sequence(:email){|n| "example#{n}@example.com"}
    password "hogehoge123"
  end

  factory :summary do
    sequence(:title) {|n| "Summary Title #{n}" }
    sequence(:description) {|n| "Summary Description #{n}" }
    user
    association :board, factory: :board_with_comments
    publish true
    refered_count 0
    items {
      []
    }

    factory :summary_with_tags do
      after(:create) do |summary|
        3.times do
          summary.tags << FactoryGirl.create(:summary_tag)
        end
      end
    end
  end

  factory :board_tag do
    sequence(:name) {|n| "board tag #{n}" }
  end

  factory :summary_tag do
    sequence(:name) {|n| "summary tag #{n}" }
  end

  factory :comment_tag do
    sequence(:name) {|n| "comment tag #{n}" }
  end

  factory :board_favorite do
    user
    board
  end

  factory :summary_favorite do
    user
    summary
  end

  factory :configuration do
    trait :default_name do
      key :default_name
      value "名無しさん"
    end

    factory :config_default_name, traits: [:default_name]
  end

  factory :black_host do
    sequence(:target) {|n| "target #{n}" }
    sequence(:reporter) {|n| "reporter #{n}" }
  end

  factory :board_observer do
    after(:create) do |bo|
      bo.boards << FactoryGirl.create(:board)
    end
  end

end

