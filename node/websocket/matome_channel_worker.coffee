SocketObserver = require('./socket_observer').SocketObserver
BoardListener = require('./board_listener').BoardListener
TagListener = require('./tag_listener').TagListener

class MatomeChannelWorker

  @start: (name, host, port) ->
    return @_worker if @_worker
    @_worker = new MatomeChannelWorker(name, host, port)
    @_worker.start()
    @_worker

  constructor: (name, host, port) ->
    @ended = false
    @resque = require('coffee-resque').connect({
      host: host,
      port: port,
      timeout: 300
    })

    # define jobs
    @jobs =
      "BoardChangeNotify": (observe_ids, json, callback) =>
        @board_change_notify(observe_ids, json, callback)
      "TagChangeNotify": (observe_ids, json, callback) =>
        @tag_change_notify(observe_ids, json, callback)

    # registor job worker
    name = "#{name}:"if name.length > 0
    @worker = @resque.worker "#{name}board_change_notify,#{name}tag_change_notify", @jobs

    # registor event 
    @worker.on 'poll', (worker, queue) => @onPoll(worker, queue)
    @worker.on 'job', (worker, queue, job) => @onJob(worker, queue, job)
    @worker.on 'error', (err, worker, queue, job) => @onError(err, worker, queue, job)
    @worker.on 'success', (worker, queue, job) => @onSuccess(worker, queue, job)

  board_change_notify: (observe_ids, json, callback) ->
    for oid in observe_ids
      ob = BoardListener.listeners[oid]
      ob.send json if ob
    callback?()

  tag_change_notify: (observe_ids, json, callback) ->
    for oid in observe_ids
      ob = TagListener.listeners[oid]
      ob.send json if ob
    callback?()

  notify_change_refersers: (observer_ids, data) ->
    for wss in mch_config.ws_servers
      @resque.enqueue("#{wss.name}:board_change_notify", 'BoardChangeNotify', [observer_ids, data])

  start: ->
    logger.info "MatomeChannelWorker started."
    @worker.start()

  end: ->
    @ended = true

  onPoll: (worker, queue)->
    return unless @ended
    logger.info "exited"
    @worker.end()
    process.exit()

  onJob: (worker, queue, job)->
    logger.debug "-- recv job: #{queue}"

  onError: (err, worker, queue, job)->
    logger.error "-- error: #{err}"
    logger.error job

  onSuccess: (worker, queue, job)->
    logger.debug "-- success job: #{queue}"

exports.MatomeChannelWorker = MatomeChannelWorker
