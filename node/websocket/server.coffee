util = require 'util'
opts = require 'opts'
 
opts.parse [{
  short: 'p'
  long: 'ws-port'
  description: 'ws server port'
  value: true
  required: false
},{
  short: 'n'
  long: 'server-name'
  description: 'ws server name'
  value: true
  required: false
},{
  short: 'P'
  long: 'redis-port'
  description: 'redis server port'
  value: true
  required: false
},{
  short: 'H'
  long: 'redis-host'
  description: 'redis host name'
  value: true
  required: false
},{
  long: 'pidfile'
  description: 'process id file (default: ./ws_[name].pid)'
  value: true
  required: false
},{
  short: 'e'
  long: 'environment'
  description: 'environment development(default) or test or production'
  value: true
  required: false
},{
  short: 'v'
  long: 'verbose'
  description: 'enable debug mode'
  value: false
  required: false
}], true

wport = opts.get('ws-port') || 8888
name = opts.get('server-name') || ""
rhost = opts.get('redis-host') || "localhost"
rport = opts.get('redis-post') || 6379
log_level = if opts.get('verbose') then "DEBUG" else "INFO"
pidfile = opts.get('pidfile') || "./ws_#{name}.pid"
env = opts.get('environment') || "development"

Yaml = require 'yamljs'
global.mch_config = Yaml.load("config/settings.yml")[env]
global.mongo_config = Yaml.load("config/mongoid.yml")[env]

# logger
global.logger = require('log4js').getLogger(name)
logger.setLevel(log_level)

# pidfile check
fs = require('fs')
if fs.existsSync(pidfile)
  logger.error "daemon already running."
  process.exit()

fs.writeFileSync pidfile, process.pid

Array.prototype.remove = (from, to) ->
  rest = this.slice((to || from) + 1 || this.length)
  this.length = from < 0 ? this.length + from : from
  return this.push.apply(this, rest)

SocketObserver = require('./socket_observer').SocketObserver
MatomeChannelWorker = require('./matome_channel_worker').MatomeChannelWorker
MatomeChannelWsServer = require('./matome_channel_ws_server').MatomeChannelWsServer

worker = MatomeChannelWorker.start(name, rhost, rport)
ws = MatomeChannelWsServer.start(wport, worker)

process.on 'SIGTERM', () ->
  logger.info "SIGTERM recv"
  SocketObserver.terminate()
  worker.end()
  fs.unlink pidfile

process.on 'SIGINT', () ->
  logger.info "SIGINT recv"
  SocketObserver.terminate()
  worker.end()
  fs.unlink pidfile

