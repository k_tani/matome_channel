Model = require("./lib/model/load")
TagObserver = Model.TagObserver

class TagListener
  @listeners = {}

  @terminate: () ->
    for obid, instance of @listeners
      instance.destroy()

  constructor: (@socket_observer, @board_tag_id, @comment_tag_id, @summary_tag_id) ->
    @observer_id = undefined
    TagObserver.create {
      board_tag_id: @board_tag_id
      comment_tag_id: @comment_tag_id
      summary_tag_id: @summary_tag_id
    }, (err, ob) =>
      throw err if err
      @observer_id = ob.id
      TagListener.listeners[ob.id] = this

  stop: () ->
    return unless @observer_id
    TagObserver.findByIdAndUpdate @observer_id, {$pull: {board_ids: @board_id}}, (err, observer) =>
      logger.error err if err

  destroy: () ->
    @stop()
    TagObserver.findByIdAndRemove @observer_id, (err, ob) =>
      logger.error err if err
    @socket_observer.socket.close()
    delete TagListener.listeners[@observer_id]

  send: (data) ->
    @socket_observer.send data

exports.TagListener = TagListener

