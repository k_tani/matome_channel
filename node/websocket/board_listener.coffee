Model = require("./lib/model/load")
Board = Model.Board
BoardObserver = Model.BoardObserver
TagObserver = Model.TagObserver

class BoardListener
  @listeners = {}

  @terminate: () ->
    for obid, listener of @listeners
      listener.destroy()

  constructor: (@socket_observer, @board_id) ->
    @observer_id = undefined
    BoardObserver.create {board_ids: [@board_id]}, (err, ob) =>
      throw err if err
      @observer_id = ob.id
      BoardListener.listeners[ob.id] = this
      Board.findByIdAndUpdate @board_id, {$push: {observer_ids: @observer_id}}, (err, board) =>
        logger.error err if err
        @notify_change_refersers(board)

  stop: () ->
    return unless @observer_id
    Board.findByIdAndUpdate @board_id, {$pull: {observer_ids: @observer_id}}, (err, board) =>
      logger.error err if err
      @notify_change_refersers(board)
    BoardObserver.findByIdAndUpdate @observer_id, {$pull: {board_ids: @board_id}}, (err, observer) =>
      logger.error err if err

  destroy: () ->
    @stop()
    BoardObserver.findByIdAndRemove @observer_id, (err, ob) =>
      logger.error err if err
    @socket_observer.socket.close()
    delete BoardListener.listeners[@observer_id]

  send: (data) ->
    @socket_observer.send data

  notify_change_refersers: (board) ->
    data = JSON.stringify
      type: "change_referers"
      count: board.observer_ids.length
    @socket_observer.worker.notify_change_refersers board.observer_ids, data

exports.BoardListener = BoardListener

