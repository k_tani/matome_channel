mongo = require 'mongoose'

# define models 
Schema = mongo.Schema
ObjectId = Schema.ObjectId

BoardSchema = new Schema
  category_id:
    type: ObjectId
  configs:
    type: Array
  observer_ids:
    type: [ObjectId]
  tag_ids:
    type: Array
  title:
    type: String
  count:
    type: Number
  score:
    type: Number
  created_at:
    type: Date
  updated_at:
    type: Date

Board = mongo.model 'board', BoardSchema

exports.Board = Board

