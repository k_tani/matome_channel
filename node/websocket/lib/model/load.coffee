mongo = require 'mongoose'
host = mongo_config.sessions.default.hosts[0]
database = mongo_config.sessions.default.database

uri = "mongodb://#{host}/#{database}"
logger.info "connect to: #{uri}"
mongo.connect uri

module.exports =
  Board: require("./board").Board
  BoardObserver: require("./board_observer").BoardObserver
  TagObserver: require("./tag_observer").TagObserver

