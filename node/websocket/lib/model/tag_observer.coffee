mongo = require 'mongoose'

# define models 
Schema = mongo.Schema
ObjectId = Schema.ObjectId

TagObserverSchema = new Schema
  board_tag_id:
    type: ObjectId
  comment_tag_id:
    type: ObjectId
  summary_tag_id:
    type: ObjectId

TagObserver = mongo.model 'tag_observer', TagObserverSchema

exports.TagObserver = TagObserver

