mongo = require 'mongoose'

# define models 
Schema = mongo.Schema
ObjectId = Schema.ObjectId

BoardObserverSchema = new Schema
  board_ids:
    type: [ObjectId]

BoardObserver = mongo.model 'board_observer', BoardObserverSchema

exports.BoardObserver = BoardObserver

