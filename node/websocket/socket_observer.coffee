util = require 'util'
BoardListener =  require("./board_listener").BoardListener
TagListener =  require("./tag_listener").TagListener

class SocketObserver

  @terminate: () ->
    BoardListener.terminate()
    TagListener.terminate()

  constructor: (@socket, @worker) ->
    # define protcol
    @types =
      start_observe_board:
        params: ["board_id"]
        callback: (board_id) =>
          @startObserveBoard(board_id)
      start_observe_tag:
        params: ["board_tag_id", "comment_tag_id", "summary_tag_id"]
        callback: (board_tag_id, comment_tag_id, summary_tag_id) =>
          @startObserveTag(board_tag_id, comment_tag_id, summary_tag_id)
    @listener = undefined
    # registor events
    @socket.on 'message', (message) => @onMessage(message)
    @socket.on 'close', () => @onClosed()
    @socket.on 'error', (e) => @onError(e)

  onError: (e) ->
    logger.error e

  onClosed: () ->
    logger.debug "closed."
    @destroy()

  onMessage: (message) ->
    @dispatch JSON.parse(message)

  send: (data) ->
    if @socket
      logger.debug "send:"
      logger.debug data
      @socket.send data
      
  destroy: () ->
    @listener?.destroy()

  dispatch: (json) ->
    try
      logger.debug "onMessage:"
      logger.debug json
      throw "invalid json #{json}" if typeof json != 'object'
      throw "invalid type #{json.type}" unless dispatcher = @types[json.type]
      throw "parameters is unset  #{json.type}" if dispatcher.params.length > 0 && !json.params
      for key in dispatcher.params
        throw "parameters:#{key} isn't set" unless json.params[key]
      dispatcher.callback json.params
    catch e
      logger.error util.inspect(e)

  startObserveBoard: (params) ->
    @listener = new BoardListener(this, params.board_id)

  stopObserveBoard: () ->
    @listener?.destroy()

  startObserveTag: (params) ->
    @listener = new TagListener(this, params.board_tag_id, params.comment_tag_id, params.summary_tag_id)

  stopObserveTag: () ->
    @listener?.destroy()

exports.SocketObserver = SocketObserver

