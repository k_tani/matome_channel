ws = require 'websocket.io'
SocketObserver = require('./socket_observer').SocketObserver

class MatomeChannelWsServer

  @start: (port, worker) ->
    return @_server if @_server
    @_server = new MatomeChannelWsServer(port,worker)
    @_server

  constructor: (port, @worker) ->
    @connections = []
    @server = ws.listen port, () =>
      logger.info "websocket server(#{port}) start."
    @server.on 'connection', (socket) => @onConnected(socket)

  onConnected: (socket) ->
    logger.info "connected."
    new SocketObserver(socket, @worker)

exports.MatomeChannelWsServer = MatomeChannelWsServer

