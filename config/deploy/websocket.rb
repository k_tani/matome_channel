Capistrano::Configuration.instance.load do

  namespace :websocket do

    set :ws_servers, [
      {name: :ws1, port: 8000},
      {name: :ws2, port: 8001},
      {name: :ws3, port: 8002},
    ]

    desc "start websocket server"
    task :start, :roles => :websocket do
      coffee = "coffee"
      logfile = File.join(current_path, "log", "websocket.log")
      script =  File.join(current_path,"node","websocket","server.coffee")
      ws_servers.each do |server|
        name = server[:name]
        port = server[:port]
        pidfile =  File.join(current_path,"tmp","pids","#{name}.pid")
        run "#{coffee} #{script} -n #{name} -p #{port} --pidfile #{pidfile} >> #{logfile} 2>&1 &"
      end
    end

    desc "stop websocket server"
    task :stop, :roles => :websocket do
      ws_servers.each do |server|
        name = server[:name]
        port = server[:port]
        pidfile =  File.join(current_path,"tmp","pids","#{name}.pid")
        run "kill -TERM `cat #{pidfile}`"
      end
    end

    desc "restart websocket server"
    task :restart, :roles => :websocket do
      stop
      start
    end
  
  end

end

