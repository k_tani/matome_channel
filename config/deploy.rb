require 'capistrano_colors'
require "bundler/capistrano"

# RVM
require "rvm/capistrano"
set :rvm_ruby_string, '2.0.0'
set :rvm_type, :user
set :rvm_path, '/usr/local/rvm'
set :rvm_bin_path, "#{rvm_path}/bin"
set :rvm_lib_path, "#{rvm_path}/lib"

set :default_run_options, :pty => true

set :application, "matome_channel"
set :repository,  "git@bitbucket.org:k_tani/matome_channel.git"

set :user, "root"
set :scm, :git
set :branch, "master"
set :deploy_via, :copy
set :deploy_to,   "/var/www/#{application}"

set :rails_env, "production"

role :web, "mt-ch"                          # Your HTTP server, Apache/etc
role :app, "mt-ch"                          # This may be the same as your `Web` server
role :db,  "mt-ch", :primary => true # This is where Rails migrations will run

# mongoid
require "./config/deploy/mongoid.rb"
after "deploy:update_code", "mongoid:copy"
after "deploy:update_code", "mongoid:symlink"
after "deploy:update", "mongoid:index"

# websocket
require "./config/deploy/websocket.rb"
role :websocket, "mt-ch"

after "deploy:stop",    "websocket:stop"
after "deploy:start",   "websocket:start"
after "deploy:restart", "websocket:restart"

# resque
require "capistrano-resque"
role :resque_worker, "mt-ch"
set :workers, { "comment_parse" => 3, "get_http_content" => 5, "create_thumbnail" => 3 }

after "deploy:stop",    "resque:stop"
after "deploy:start",   "resque:start"
after "deploy:restart", "resque:restart"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

namespace :myapp do
  task :npm_update do
    put File.read("package.json"), "#{shared_path}/package.json"
    run "cd #{shared_path} && npm install"
  end

  task :setup do
    put File.read("config/settings.yml"), "#{shared_path}/config/settings.yml"
  end

  task :symlink_config do
    run "ln -nfs #{shared_path}/config/settings.yml #{release_path}/config/settings.yml"
    run "ln -nfs #{shared_path}/node_modules #{release_path}/node_modules"
  end

  after "deploy:update", "myapp:npm_update"
  after "deploy:setup", "myapp:setup"
  before "deploy:assets:symlink", "myapp:symlink_config"

end


