# -*- coding: utf-8 -*-
MatomeChannel::Application.routes.draw do

  devise_for :users, :controllers => {
    :registrations => "registrations",
    :omniauth_callbacks => "users/omniauth_callbacks"
  }

  devise_scope :user do
    get 'login', :to => 'devise/sessions#new', :as => :new_user_session
    delete 'logout', :to => 'devise/sessions#destroy', :as => :destroy_user_session
  end

  resources :members, only: [] do
    member do
      get "board_favorites"
      get "summary_favorites" 
      get 'boards'
      get 'summaries'
    end
  end

  resources :categories, only: [] do
    member do
      resources :boards, as: :category_boards , only: [:index, :new] do
        collection do
          match "t/:tag_id" => "boards#tag", as: :tag, via: :get
        end
      end
      resources 'summaries', as: :category_summaries, only: [:index] do
        collection do
          match "t/:tag_id" => "summaries#tag", as: :tag, via: :get
        end
      end
    end
  end

  resources :boards, only: [:index, :show, :new, :create]do
    collection do
      match "t/:tag_id" => "boards#tag", as: :tag, via: :get
    end
    member do
      match 'comments/:num' => "boards#comments", as: :comments, via: :get
      match 'websites/:num' => "boards#websites", as: :websites, via: :get
      match 'images/:num' => "boards#images", as: :images, via: :get
      match 'movies/:num' => "boards#movies", as: :movies, via: :get
      post :tag, :action => :add_tag
      delete :tag, :action => :remove_tag
    end
  end

  resources :comments, only: [:show,:create] do
    member do
      post :good
      post :bad
      post :ng, :action => :add_ng
      delete :ng, :action => :remove_ng
      post :tag, :action => :add_tag
      delete :tag, :action => :remove_tag
    end
    resources :t, controller: "comment_tags", except: [:index, :edit, :update]
  end

  resources :websites, only: [] do
    member do
      get :thumb
    end
  end

  resources :images, only: [:create] do
    member do
      get :image
      get :thumb
    end
  end

  resources :movies, only: [] do
    member do
      get :animation
      get :thumb
    end
  end

  resources :summaries, except: [:new, :destroy] do
    collection do
      match "t/:tag_id" => "summaries#tag", as: :tag, via: :get
      match 'new/:board_id' => 'summaries#new', via: :get, as: :new
      post 'items'
    end
  end

  resources :favorites, only: [:index, :destroy] do
    collection do
      get 'boards'
      get 'summaries'
      post 'boards', action: :create_board
      post 'summaries', action: :create_summary
      delete 'boards/:id', action: :destroy_board
      delete 'summaries/:id', action: :destroy_summary
    end
  end


  scope 'my' do
    get 'boards', controller: "histories", action: "my_boards", as: :my_boards
    get 'comments', controller: "histories", action: "my_comments", as: :my_comments
    get 'summaries', controller: "histories", action: "my_summaries", as: :my_summaries
  end

  scope 'histories' do
    get 'boards', controller: "histories", action: "boards", as: :boards_history
    get 'summaries', controller: "histories", action: "summaries", as: :summaries_history
  end

  resource :ng_list, only: [:show]
  resource :check_list, only: [:show]
  resources :tags, only: [] do
    collection do
      get 'board'
      get 'summary'
      get 'observe', action: :observe
    end
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'

  root :to => 'top#index'

  match '*a', :to => 'application#error_404'

end
