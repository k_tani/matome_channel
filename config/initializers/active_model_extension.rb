module ActiveModel
  class Errors
    def to_json_local
      json = {}
      self.each do |attr, error|
        json[attr] ||= []
        json[attr] << full_message(attr,error)
      end
      json
    end
  end
end

