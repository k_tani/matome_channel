class String
  def mlength
    len = 0
    self.each_char do |c|
      Moji.type?(c,Moji::ZEN) ? len += 2 : len += 1
    end
    len
  end

  def cut(num=30,omission="...")
    ret = ""
    cnt = 0
    self.each_char do |c|
      Moji.type?(c,Moji::ZEN) ? cnt += 2 : cnt += 1
      break if cnt > num
      ret << c
    end
    ret << omission if cnt > num
    return ret
  end
end

