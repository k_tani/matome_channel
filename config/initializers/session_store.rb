# Be sure to restart your server when you modify this file.

MatomeChannel::Application.config.session_store :mem_cache_store, {
  #:cookie_only => false,
  #:expire_after => 30.days,
  :memcache_server => 'localhost:11211'
}
# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# MatomeChannel::Application.config.session_store :active_record_store
