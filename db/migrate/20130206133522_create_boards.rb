class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.integer :category_id
      t.string  :title
      t.integer :score

      t.timestamps
    end
  end
end
