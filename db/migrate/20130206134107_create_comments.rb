class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :board_id
      t.integer :parent_id
      t.string :name
      t.text :body
      t.integer :good, :default => 0
      t.integer :bad, :default => 0

      t.timestamps
    end
  end
end
