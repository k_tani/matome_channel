# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

({
  "社会" => ["社会", "政治", "経済"],
  "暮らし" => ["生活", "人生"],
  "エンターテイメント" => ["スポーツ", "音楽", "芸能"],
  "学ぶ" => ["科学", "学問"],
  "コンピュータ" => ["コンピュータ・IT"],
  "サブカルチャー" => ["アニメ", "ゲーム"],
  "おもしろ" => ["おもしろ"],
}).each do |k,v|
  p = Category.create(name: k)
  v.each do |n|
    cat = Category.new(name: n)
    cat.parent = p
    cat.save!
  end
end

